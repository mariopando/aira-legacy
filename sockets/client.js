/**
    UDP socket client for Aira project (USC)
    @Authors: CrazyCake Interactive Technologies
    @Version: 1.0
    @platform: Javascript, NodeJS
    For instructions, please read README file
**/

//++ Load Modules (global vars)
var MOD_optimist    = require('optimist').argv;
var MOD_dgram       = require('dgram');
var MOD_moment      = require('moment-timezone');
//load external Module files
var EXT_tools = require('./tools');

//++ Environment configurations
var APP_NAMESPACE    = "client";
var APP_PATH         = __dirname + "/"; //UNIX PATH SYSTEM ONLY
var DEVELOPMENT_MODE = (MOD_optimist.d ? true : false);
var TESTING_MODE     = (MOD_optimist.t ? true : false);
var SOCKET_PORT      = (MOD_optimist.p ? MOD_optimist.p : 9002); //9002 por defecto (arg -p 9003)
//host
var HOST;
if(DEVELOPMENT_MODE)
    HOST = "127.0.0.1";
else if(TESTING_MODE)
    HOST = "54.85.185.121";
else
    HOST = "54.85.185.121";

//++ APP configurations
var APP = {
    HOST        : HOST,
    SOCKET_PORT : SOCKET_PORT,
    TIME_ZONE   : "America/Santiago",
    FOLDERS     : {
                    LOGS : APP_PATH + APP_NAMESPACE + "_logs/"
                }
};

//Set message for testing
var MESSAGE =  '[{"a":1},{"n":"test","v":"1;2"},{"n":"tiempo","v":"09/09/19;9:9:9"}]';
//'[{"a":1},{"n":"coord","v":"-3328.9248,-7033.2124;-3328.9250,-7033.2124"},{"n":"velocidad","v":"15.8;18.9;22.8;24.4;2.0;1.0;0;5;9;10;14;12.5"},{"n":"tpo. encendido","v":"596;601;606;611;616;621"},{"n":"RPM","v":"796;704;813;725;768;737"},{"n":"tiempo","v":"21/06/15;23:9:49"}]';

//++ Create Folders
EXT_tools.folder_mkdir(APP.FOLDERS);
//++ LOG SYSTEM
var logger_name = APP.FOLDERS.LOGS + MOD_moment().tz(APP.TIME_ZONE).format("DD_MM_YYYY") + ".log";
var logger      = EXT_tools.logger_config(logger_name, APP_NAMESPACE);

//++ Socket Client
var message = new Buffer(MESSAGE);
var client  = send_socket_message(APP.HOST, APP.SOCKET_PORT, message);

/*----------------------------------------------------------------------------------------------------------------------
    Init tasks
------------------------------------------------------------------------------------------------------------------------*/
function send_socket_message(host, port, message)
{
    var client   = MOD_dgram.createSocket("udp4");
    var now      = MOD_moment().tz(APP.TIME_ZONE).format("DD/MM/YYYY HH:mm");

    client.send(
        message,
        0, //buffer offset
        message.length,
        port,
        host,
        function(err, bytes)
        {
            if (err)
                throw err;

            logger.info("Message sent: " + message + "\nTo: " + host + "(" + port + ")\n");

            //close socket
            client.close();
        }
    );

    //return socket
    return client;
}
