/**
 * Helper tools
 * @author: Nicolas Pulido <nicolas.pulido@crazycake.cl>
 * Dependencies: NodeJS
 * Required Modules: jsdom, jquery, mandrill-api
 */

//-- Modules
var MOD_fs       = require("fs");
var MOD_log4js   = require('log4js');
//Mandrill API
var MOD_mandrill = require('mandrill-api/mandrill');

//external functions
var functions = {

    /**
     * Loger Config
     * @param  {string} fullpath
     * @param  {string} namespace
     * @return {object} The Logger object
     */
    logger_config : function(fullpath, namespace) {

        //delete log file first if exits
        if( MOD_fs.existsSync(fullpath) ) {
            //console.log("Tools -> Removing log file %s ...", fullpath);
            MOD_fs.unlink(fullpath);
        }

        MOD_log4js.configure({
            appenders: [
                {
                    type : "file",
                    filename : fullpath,
                    category : [namespace, 'console']
                },
                {
                    type : "console"
                }
            ],
            replaceConsole: true
        });
        //return logger object
        return MOD_log4js.getLogger(namespace);
    },

    /**
     * File & Folder Tools
     * @param  {string} fullpath
     */
    file_exists : function(fullpath) {

        if(MOD_fs.existsSync(fullpath)) {

            var stats = MOD_fs.statSync(fullpath);
            var fileSizeInBytes = stats.size;
            //check in KiloBytes
            return ((fileSizeInBytes / 1024) > 1 ? true : false);
        }
        else {
            return false;
        }
    },

    /**
     * Creates folders
     * @param  {object} folders_obj
     * @param  {boolean} force_exit
     */
    folder_mkdir : function(folders_obj, force_exit)
    {
        if(typeof folders_obj != "object")
            return;

        if(typeof force_exit == "undefined")
            force_exit = true;

        var error_fn = function(err) {
            //ignore exists error
            if(err !== null && err.code != "EEXIST") {
                console.log("APP Init (folder_mkdir) -> Error creating folder %s. Error: %s", path, err);

                if(force_exit)
                    process.exit(1);
            }
        };

        for(var obj in folders_obj) {
            //set path
            var path = folders_obj[obj];

            if(!MOD_fs.existsSync(path))
                MOD_fs.mkdir(path, error_fn);
        }
    },

    /**
     * Sends a mail message through mandrill client
     * @param  {string} msg
     */
    send_mail_message : function(msg)
    {
        //-- Mandrill API
        var mandrill_client = new MOD_mandrill.Mandrill('RE3z78hfRD-qJm4EIlVnVw');

        var message = {
                "html": "<p><b>Mensaje</b></p><p>"+msg+"</p><br/><br/><p><i>CrazyCake DevTeam</i></p>",
                "text": "Mensaje: \n"+msg,
                "subject": "Socket Notification (Aira CrazyCake)",
                "from_email": "dev@crazycake.cl",
                "from_name": "Socket",
                "to": [{
                        "email": "crazycake.dev@gmail.com",
                        "name": "CrazyCake Dev"
                    }],
                "headers": {
                    "Reply-To": "contacto@crazycake.cl"
                }
        };

        var async   = false;
        var ip_pool = "Main Pool";
        var send_at = null;
        //send email
        mandrill_client.messages.send({"message": message, "async": async, "ip_pool": ip_pool, "send_at": send_at},
        function(response) {
            //console.log("send_mail_message -> Response:", response);
        },
        function(e) {
            // Mandrill error
            console.log('send_mail_message -> A mandrill error occurred: ' + e.name + ' - ' + e.message);
        });
    }
};

/* exports module */
module.exports = functions;
