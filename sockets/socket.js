/**
 * UDP socket server for Aira project (USS)
 * @author: Nicolas Pulido <nicolas.pulido@crazycake.cl>
 * @version: 1.0
 * Dependencies: NodeJS (npm)
 * For instructions, see README file.
 */

//++ Load Modules (global vars)
var MOD_optimist 	= require('optimist').argv;
var MOD_request 	= require("request");
var MOD_dgram		= require('dgram');
var MOD_moment 		= require('moment-timezone');
//load external Module files
var EXT_tools = require('./tools');

//++ Environment configurations
var APP_NAMESPACE    = "socket";
var APP_PATH 		 = __dirname + "/"; //UNIX PATH SYSTEM ONLY
var DEVELOPMENT_MODE = (MOD_optimist.d ? true : false);
var TESTING_MODE	 = (MOD_optimist.t ? true : false);
var SOCKET_PORT      = (MOD_optimist.p ? MOD_optimist.p : 9002); //9002 por defecto (arg -p 9003)

//++ APP configurations
var APP = {
	SOCKET_PORT : SOCKET_PORT,
	FOLDERS     : {
		LOGS : APP_PATH + APP_NAMESPACE + "_logs/"
	},
	//Local API
	LOCAL_API_URL   : (DEVELOPMENT_MODE ? "http://localhost/aira-webapp/api/" : (TESTING_MODE ? "http://aira.api.m1.testing.crazycake.cl/" : "http://aira.api.m1.testing.crazycake.cl/" )),
	LOCAL_API_PATHS : {
		"welcome" 	   : { path : "./", method : "GET" },
		"save_metrics" : { path : "./metrics/save_batch", method : "POST" }
	}
};

//++ Create Folders
EXT_tools.folder_mkdir(APP.FOLDERS);
//++ LOG SYSTEM
var logger_name = APP.FOLDERS.LOGS + MOD_moment().format("DD_MM_YYYY") + ".log";
var logger 		= EXT_tools.logger_config(logger_name, APP_NAMESPACE);

//++ Init Server
var socket = init_server(APP.SOCKET_PORT);

return "Ok, script executed.";

/**
 * Init Server
 * @param  {int} port
 */
function init_server(port) {

	var socket   = MOD_dgram.createSocket("udp4");
	var env_name = (DEVELOPMENT_MODE ? 'Development' : (TESTING_MODE ? 'Testing' : 'Production'));
	var now  	 = MOD_moment().format("DD/MM/YYYY HH:mm");

	//Listen for message events on the socket
	socket.on("message", function (message, requestInfo) {
        //Log the received message.
        logger.info("Message Received: " + message + "\nFrom:" + requestInfo.address + "(" + requestInfo.port + ")\n");

        if(typeof message == "undefined" || message.constructor.name != "Buffer") {
        	logger.warn("Invalid message buffer");
        	return;
        }
        //transform buffer to utf8 string
        var data = message.toString('utf8'); //ascii

        //local_api_request(APP.LOCAL_API_PATHS['welcome']);
        local_api_request(APP.LOCAL_API_PATHS.save_metrics, data);
	});

	//Listen for error events on the socket.
	//When we get an error, we want to be sure to CLOSE the socket;
	socket.on("error", function (error) {
 		logger.info("Socket error: " + error);
 		//close socket
        socket.close();
        //exit process
        process.exit(1);
    });

	//Listening event
	socket.on("listening", function() {
		//set host
		var host = socket.address();
		//console.log(host);
		logger.info('\nUDP Socket Server Listening...\nHOST: %s \nPORT: %s \nMODE: %s \nPATH: %s \nLOCAL_API_URL: %s \nTIMESTAMP: %s \n',
			host.address, port, env_name, APP_PATH, APP.LOCAL_API_URL, now);
	});

	//Start listening on the given port.
	socket.bind(port);
	//return socket
	return socket;
}

/*
 API requests
---------------------------------------------------------------------------------------------------------------------*/

/**
 * Local API request
 * api object is an object with {path, method} struct
 * @param {object} api_object
 * @param {object} params
 */
function local_api_request(api_object, params) {

	if(typeof params == "undefined")
		params = {};
	else if(typeof params == "string")
		params = { payload : params };

	var request_url = APP.LOCAL_API_URL + api_object.path.replace("./", "");
	//set request config
	var request_conf = {
    	uri 	 : request_url,
		method 	 : api_object.method,
		encoding : 'utf-8',
    	headers  : {
					"Accept" 	 : "application/json",
					'User-Agent' : "Aira USS"
    			 },
    	form 	 : params,
    	timeout  : 1 * 60 * 1000 //1 minute.
	};

	logger.info("Requesting local API ("+api_object.method+"): " + request_url + " ...\n");

	//make the request
	MOD_request(request_conf, function (error, response, body) {
		//handle response
		try {
			var data = _handle_local_api_response(api_object, error, response, body);

			logger.trace("Request response successful ("+api_object.method+"): " + request_url + "\nData:", data);
		}
		catch (err) {
			logger.warn("local_api_request -> An error ocurred! Error: "+err);
		}
	});
}

/**
 * Parse the API response
 * @param {object} api_object
 * @param {object} error
 * @param {object} response
 * @param {object} body
 */
function _handle_local_api_response(api_object, error, response, body) {

	//check response
	if(error || typeof response == "undefined" || response.statusCode != 200) {
		throw "Failed to reach server or failed request.\nRequest: "+api_object.path+" ("+api_object.method+")\nStatusCode:"+response.statusCode;
	}

	var data = null;
	//parse data
	try {
		data = JSON.parse(body);
		data = data.response; //root
	}
	catch (err) {
		throw "Invalid body JSON format.\nRequest: "+api_object.path+" ("+api_object.method+")\nBody:"+body;
	}

	if(typeof data.status != "string" || (data.status != "error" && data.status != "ok"))
		throw "Invalid response struct JSON format.\nRequest: "+api_object.path+" ("+api_object.method+")";

	//response has an API error
	if(data.status == "error")
		throw "The API responded with an error: '"+data.error+"'.\nRequest: "+api_object.path+" ("+api_object.method+")";

	var payload = true;
	if(typeof data.payload != "undefined")
		payload = data.payload;

	return payload;
}
