#! /bin/bash
#-- Node app executer

set -e
#Current path
CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
APP_NAME="Aira"
# WebApp properties
APP_ENV=""
FILE_PATH=$CURRENT_PATH"/socket.js"

#script help function
scriptHelp() {
	echo -e "\033[93m"$APP_NAME" UDP Socket Script\nValid commands:\033[0m"
	echo -e "\033[95m install: Install node dependencies.\033[0m"
	echo -e "\033[95m start: Start node with forever.\033[0m"
	echo -e "\033[95m stop: Stop node already started with forever.\033[0m"
	echo -e "\033[95m restart: Restart node with forever.\033[0m"
	echo -e "\033[95m list: Show all forever process running.\033[0m"
	exit
}

# check args
if [ "$*" = "" ]; then
	scriptHelp
fi

# set app env (use -c for forever sub args)
if [ "$2" == "-d" ] || [ "$2" == "-t" ]; then
	APP_ENV="-c "$2
fi

if [ $1 = "install" ]; then

	echo -e "\033[96mInstalling node dependencies... \033[0m"
	cd $CURRENT_PATH
	npm install

elif [ $1 = "start" ]; then

	# print project dir
	echo -e "\033[96mProject Dir: "$CURRENT_PATH" \033[0m"
	echo -e "\033[95mStarting node app... \033[0m"
	forever start $FILE_PATH $APP_ENV

elif [ $1 = "stop" ]; then

	#print project dir
	echo -e "\033[96mProject Dir: "$CURRENT_PATH" \033[0m"
	echo -e "\033[95mStopping node app... \033[0m"
	forever stop $FILE_PATH $APP_ENV

elif [ $1 = "restart" ]; then

	# print project dir
	echo -e "\033[96mProject Dir: "$CURRENT_PATH" \033[0m"
	echo -e "\033[95mRestarting node app... \033[0m"
	forever stop $FILE_PATH $APP_ENV
	forever start $FILE_PATH $APP_ENV

elif [ $1 = "list" ]; then

	# print forever process
	forever list
else

	echo -e "\033[31mInvalid command\033[0m"
fi
