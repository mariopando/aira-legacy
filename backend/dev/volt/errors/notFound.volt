{# NotFound View #}

<h1 class="text-center">{{ trans._("Oops, ha ocurrido algo") }}</h1>

<p>{{ trans._("Esta página no ha sido encontrada.") }}</p>
