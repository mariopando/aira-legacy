{# SignIn View #}
<div id="vue-auth" class="app-sign-in-wrapper app-form">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name" style="font-size:120px;">AIRA</h1>
            </div>

            <form class="m-t" role="form" data-validate v-on:submit="loginUserByEmail">
                <div class="form-group">
                    <input name="email" type="email" class="form-control" maxlength="255" v-model="loginInputEmail" autocomplete="on" placeholder="{{ trans._('Correo electrónico') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
                </div>
                <div class="form-group">
                    <input name="pass" type="password" class="form-control" maxlength="20" autocomplete="off" placeholder="{{ trans._('Contraseña') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa tu contraseña.') }}" />
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans._('Ingresar') }}</button>

                <a href="{{ url('password/recovery') }}"><small>{{ trans._('¿Olvidaste tu contraseña?') }}</small></a>
            </form>

            <p class="m-t"> <small>{{ trans._('Powered by %a_open%CrazyCake%a_close%', ['a_open' : '<a href="http://www.crazycake.cl">', 'a_close' : '</a>']) }}</small> </p>
        </div>
    </div>

    <!-- Button trigger modal
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
      Launch demo modal <i class="fa fa-search"></i>
    </button> -->
    <!-- Modal
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            {# model content #}
            <div class="modal-content">
                {# form #}
                <form data-validate data-fv-live="disabled" v-on:submit="resendActivationMailMessage">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ trans._('Correo de activación') }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group" style="margin:0;">
                            {# recaptcha #}
                            <div class="recaptcha">
                                <small>{{ trans._('Cargando Recaptcha...') }}</small>
                                <div id="app-recaptcha"></div>
                                <input name="reCaptchaValue" type="hidden"
                                       data-fv-excluded="false" data-fv-required="numeric : {}"
                                       data-fv-message="{{ trans._('Completa el reCaptcha.') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans._('Cancelar') }}</button>
                        <button type="submit" class="btn btn-primary">{{ trans._('Aceptar') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>-->

</div>
