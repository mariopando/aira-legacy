/**
 * App.js
 * Browserify can only analyze static requires.
 * It is not in the scope of browserify to handle dynamic requires.
 */

//load bundle dependencies
require('webpack_core');

//UI framework
require('bootstrap');

//theme plugins
require('metismenu');
require('slimscroll');
require('peity');
require('flot');
require('flot.time');
require('google-maps');
require('jquery-sparkline');

var modules = [
    new (require('./src/ui.js'))(),
    new (require('./src/account.js'))()
];

//set modules
core.setModules(modules);
