/** ---------------------------------------------------------------------------------------------------------------
    UI Module
---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {

    //self context
    var self        = this;
    self.moduleName = "ui";

    //++ UI Selectors
    _.assign(APP.UI, {
        //settings
        alert : {
            position    : "fixed",
            top         : "belowHeader",
            topForSmall : "0"
        },
        loading : {
            position    : "fixed",
            top         : "14%",
            topForSmall : "20%",
            center      : true
        },
        //selectors
        sel_header_profile : "#app-header-profile",
        sel_header_menu    : "#app-header-menu"
    });

    /**
     * Init function
     */
    self.init = function() {

        //app load UI
        self.uiSetUp();

    };

    /**
     * App Theme UI
     */
    self.uiSetUp = function() {

    };
};
