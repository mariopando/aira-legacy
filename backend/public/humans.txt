/* MAIN */
 	Develop: Nicolas Pulido
 	GitHub: https://github.com/npulidom

 	Develop: Cristhoper Jaña
  
 	Design: Jesús López & Hernán Romero
 	Site: http://www.crazycake.cl

/* SITE */
 	Doctype: HTML5

/* SPECIAL THANKS */
 	CrazyCake Team
 	And, of course, all humanstxt.org team!