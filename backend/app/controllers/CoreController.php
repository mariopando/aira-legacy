<?php
/**
 * Core Controller, parent of all app controllers, includes basic and helper methods for child controllers.
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake
use CrazyCake\Core\WebCore;

class CoreController extends WebCore
{
    /**
     * Set javascript customized properties for rendering view
     */
    protected function setAppJsProperties($js_app)
    {
        //google
        $js_app->googleReCaptchaID = $this->config->app->google->reCaptchaID;
    }

    /**
     * Check if browser is supported
     * @param string $browser
     * @param int $version
     * @return boolean
     */
    protected function checkBrowserSupport($browser, $version)
    {
        //not supported browsers
        if($browser == "MSIE"    && $version < 11) return false;
        if($browser == "Chrome"  && $version < 18) return false;
        if($browser == "Firefox" && $version < 14) return false;
        if($browser == "Opera"   && $version < 15) return false;
        if($browser == "Safari"  && $version < 5) return false;

        return true;
    }
}
