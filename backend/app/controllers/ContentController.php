<?php
/**
 * ContentController: handles Content pages
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class ContentController extends SessionController
{
    /**
     * View - Help
     */
    public function faqAction()
    {
        //...
    }

    /**
    * View - Contact page
    */
   public function contactAction()
   {
       //load js modules
       $this->_loadJsModules([
           "contents" => null
       ]);
   }
}
