{#  Auth Parent Layout
	==============================================
#}

{# Auth Wrapper #}
<div class="app-auth-wrapper">
    <div class="app-container">
		{# SubHeader account (logo) #}
		{{ partial("templates/headerForm") }}
    	{# content #}
		{{ get_content() }}
	</div>
</div>
