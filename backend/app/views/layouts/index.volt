{#  Index Parent Layout
	==============================================
#}

{# Header TopBar #}
{{ partial("templates/sidebar") }}

{# Index Wrapper #}
<div class="app-index-wrapper app-container-wrapper">
	{# content #}
	{{ get_content() }}
</div>
