{#  Account Parent Layout
	==============================================
#}

{# Header TopBar #}
{{ partial("templates/sidebar") }}

{# Account Wrapper #}
<div class="app-account-wrapper app-container-wrapper">
    {# content #}
    <div class="app-container app-account-content">
        {{ get_content() }}
    </div>
</div>
