{# HEADER TOP BAR #}

{% if user_data is defined and user_data %}
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        {#<span><img alt="image" class="img-circle" src="{% if user_data['profile_img_path'] is defined %} {{ user_data['profile_img_path']  }} {% else %} {{ static_url('images/icons/profile_small.jpg') }} {% endif %}" /></span>#}
                        <span><img alt="image" class="img-circle" src="{{ static_url('images/icons/profile_small.jpg') }}" /></span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ user_data['first_name'] }} {{ user_data['last_name'] }}</strong>
                                 </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('auth/logout') }}">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li class="active">
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Report</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{ url('report/speeding') }}">Speeding</a></li>
                        <li><a href="{{ url('report/rpm') }}">RPM</a></li>
                        <li><a href="{{ url('report/harsh_accelerations') }}">Harsh accelerations</a></li>
                        <li><a href="{{ url('report/harsh_breaking') }}">Harsh breaking</a></li>
                        <li><a href="{{ url('report/cdt') }}">CDT</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-map-marker"></i> <span class="nav-label">Map</span> </a>
                </li>
            </ul>
        </div>
    </nav>
{% endif %}
