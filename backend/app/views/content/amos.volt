<div class="row">
    <div class="col-md-3">
        <div class="widget white-bg p-xl no-padding amos">
            <div class="p-m">
                <span class="label label-success pull-right">ID 1</span>
                <h1>Amo 1</h1>
                <h5>Hyundai Accent 2015 Hatchback</h5>
                <h5>XR 77 81</h5>
            </div>
            <div id="street_view"></div>
            <div class="p-m">
                <ul class="list-unstyled m-t-md">
                    <li>
                        <span class="fa fa-tachometer m-r-xs"></span>
                        <label>Speeding:</label>
                        130 Km/h
                    </li>
                    <li>
                        <span class="fa fa-cog m-r-xs"></span>
                        <label>RPM:</label>
                        5571 rpm
                    </li>
                    <li>
                        <span class="fa fa-exclamation m-r-xs"></span>
                        <label>Harsh Accelerations</label>
                        <ul>
                            <li>February 8, 2016, at 6:43 pm</li>
                            <li>February 8, 2016, at 6:43 pm</li>
                            <li>February 8, 2016, at 6:43 pm</li>
                        </ul>
                    </li>
                    <li>
                        <span class="fa fa-exclamation-circle m-r-xs"></span>
                        <label>Harsh Accelerations</label>
                        <ul>
                            <li>March 8, 2016, at 2:16 am</li>
                            <li>March 8, 2016, at 2:16 am</li>
                            <li>March 8, 2016, at 2:16 am</li>
                        </ul>
                    </li>
                    <li>
                        <span class="fa fa-car m-r-xs"></span>
                        <label>CDT (Continuous driving time)</label><br>
                        234 Km/h
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="widget lazur-bg p-xl no-padding">
            <div class="p-m">
                <h1>
                    AMOS 2
                </h1>
                <h3>ID 2</h3>
                <h5>Hyundai Accent 2015 Hatchback</h5>
                <h5>XR 77 81</h5>
                <ul class="list-unstyled m-t-md">
                    <li>
                        <span class="fa fa-tachometer m-r-xs"></span>
                        <label>Speeding:</label>
                        130 Km/h
                    </li>
                    <li>
                        <span class="fa fa-cog m-r-xs"></span>
                        <label>RPM:</label>
                        5571 rpm
                    </li>
                    <li>
                        <span class="fa fa-exclamation m-r-xs"></span>
                        <label>Harsh Accelerations</label>
                        <ul>
                            <li>February 8, 2016, at 6:43 pm</li>
                            <li>February 8, 2016, at 6:43 pm</li>
                            <li>February 8, 2016, at 6:43 pm</li>
                        </ul>
                    </li>
                    <li>
                        <span class="fa fa-exclamation-circle m-r-xs"></span>
                        <label>Harsh Accelerations</label>
                        <ul>
                            <li>March 8, 2016, at 2:16 am</li>
                            <li>March 8, 2016, at 2:16 am</li>
                            <li>March 8, 2016, at 2:16 am</li>
                        </ul>
                    </li>
                    <li>
                        <span class="fa fa-car m-r-xs"></span>
                        <label>CDT (Continuous driving time)</label><br>
                        234 Km/h
                    </li>
                </ul>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart3"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ibox amos">
            <div class="ibox-title">
                <span class="label label-success pull-right">ID 4</span>
                <h1>Amo 3</h1>
            </div>
            <div class="ibox-content">
                <p><span class="label red">Hyundai</span>
                    <span class="label">Accent</span>
                    <span class="label">2015</span>
                    <span class="label">Hatchback</span>
                    <span class="label">XR 77 81</span></p>

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-3" id="velocity_tab"> <i class="fa fa-tachometer"></i></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-4" id="rpm_tab"><i class="fa fa-cog"></i></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-5"><i class="fa fa-exclamation"></i></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-6"><i class="fa fa-exclamation-circle"></i></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-7"><i class="fa fa-car"></i></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-8"><i class="fa fa-road"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-3" class="tab-pane active">
                        <div class="panel-body">
                            <strong>Speeding</strong>
                            <div class="text-center">
                                <h2><span id="km_label"></span> Km/h</h2>
                                <div id="sparkline_velocity"></div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-4" class="tab-pane">
                        <div class="panel-body">
                            <strong>RPM</strong>
                            <div class="text-center">
                                <h2><span id="rpm_label"></span> rpm</h2>
                                <div id="sparkline_rpm"></div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-5" class="tab-pane">
                        <div class="panel-body">
                            <strong>Harsh Accelerations</strong>
                            <table class="table table-stripped small m-t-md">
                                <tbody>
                                <tr>
                                    <td class="no-borders">
                                        <i class="fa fa-circle text-danger"></i>
                                    </td>
                                    <td  class="no-borders">
                                        February 8, 2016, at 6:43 pm
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-circle text-danger"></i>
                                    </td>
                                    <td>
                                        February 8, 2016, at 6:43 pm
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-circle text-danger"></i>
                                    </td>
                                    <td>
                                        February 8, 2016, at 6:43 pm
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tab-6" class="tab-pane">
                        <div class="panel-body">
                            <strong>Harsh Breaking</strong>
                            <table class="table table-stripped small m-t-md">
                                <tbody>
                                <tr>
                                    <td class="no-borders">
                                        <i class="fa fa-circle text-danger"></i>
                                    </td>
                                    <td  class="no-borders">
                                        March 8, 2016, at 2:16 am
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-circle text-danger"></i>
                                    </td>
                                    <td>
                                        March 8, 2016, at 2:16 am
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-circle text-danger"></i>
                                    </td>
                                    <td>
                                        March 8, 2016, at 2:16 am
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tab-7" class="tab-pane">
                        <div class="panel-body">
                            <strong>CDT (Continuous driving time)</strong>
                            <div class="text-center"><br>
                                <h1 class="no-margins">124:16</h1>
                                <small class="text-center">hours/minutes</small>
                            </div>
                        </div>
                    </div>
                    <div id="tab-8" class="tab-pane">
                        <div class="panel-body">
                            <strong>Street View</strong>
                            <div class="google-map" id="pano"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>