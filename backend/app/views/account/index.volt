{# Navigation #}
<div id="page-wrapper" class="gray-bg">
    {{ partial("templates/header") }}
    <div class="wrapper wrapper-content">
        {{ partial("content/amos") }}
        {{ partial("content/resume") }}
        {{ partial("content/map") }}
    </div>
</div>
