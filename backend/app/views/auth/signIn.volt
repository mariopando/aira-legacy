{# SignIn View #}
<div id="vue-auth" class="app-sign-in-wrapper app-form">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name" style="font-size:120px;">AIRA</h1>
            </div>

            <form class="m-t" role="form" data-validate v-on:submit="loginUserByEmail">
                <div class="form-group">
                    <input name="email" type="email" class="form-control" maxlength="255" v-model="loginInputEmail" autocomplete="on" placeholder="{{ trans._('Correo electrónico') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
                </div>
                <div class="form-group">
                    <input name="pass" type="password" class="form-control" maxlength="20" autocomplete="off" placeholder="{{ trans._('Contraseña') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa tu contraseña.') }}" />
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans._('Ingresar') }}</button>

                <a href="{{ url('password/recovery') }}"><small>{{ trans._('¿Olvidaste tu contraseña?') }}</small></a>
            </form>

            <p class="m-t"> <small>{{ trans._('Powered by %a_open%CrazyCake%a_close%', ['a_open' : '<a href="http://www.crazycake.cl">', 'a_close' : '</a>']) }}</small> </p>
        </div>
    </div>

</div>
