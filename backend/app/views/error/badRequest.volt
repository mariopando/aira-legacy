{# Bad Request View #}

<h1 class="text-center">{{ trans._("Oops, ha ocurrido algo") }}</h1>

<p>{{ trans._("No hemos logrado procesar tu petición, porfavor inténtalo en otro momento.") }}</p>
