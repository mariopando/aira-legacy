{# Internal Server Error View #}

<h1 class="text-center">{{ trans._("Oops algo ocurrió") }}</h1>

<p>{{ trans._("Esta página ha expirado y no se encuentra disponible.") }}</p>

{# show error message #}
{% if error_message is defined %}
	<p>{{ error_message }}</p>
{% endif %}
