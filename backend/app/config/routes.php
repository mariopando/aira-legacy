<?php
/**
 * Phalcon App Routes files
 * 404 error page is managed by Route 404 Plugin automatically
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

return function($router) {

	//Default route
	$router->add("/", [
	    'controller' => 'auth',
	    'action'     => 'signIn'
	]);

	//login
	$router->add("/signIn", [
	    'controller' => 'auth',
	    'action'     => 'signIn'
	]);
};
