<?php
/**
 * Phalcon Project Environment configuration file.
 * Requires PhalconPHP installed
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//include CrazyCake phalcon loader
require is_link(__DIR__."/packages/cc-phalcon") ? __DIR__."/packages/cc-phalcon/autoload.php" : __DIR__."/packages/cc-phalcon.phar";

class PhalconApp extends \CrazyCake\Phalcon\AppLoader
{
    /**
     * Required app configuration
     */
    protected function config()
    {
        return [
            //Project Path
            "projectPath" => __DIR__."/",
            //Module Settings
            "modules" => [
                "backend" => [
                    "loader"    => ["models", "api/models"],
                    "core"      => [],
                    "langs"     => ["es"],
                    "version"   => "0.0.1",
                ],
                "frontend" => [
                    "loader"    => ["models", "api/models"],
                    "core"      => [],
                    "langs"     => ["es"],
                    "version"   => "0.0.2",
                ],
                "api" => [
                    "loader"     => ["models"],
                    "core"       => [],
                    "version"    => "0.1.0",            //Api version
					"key"        => "*Cr4ZyCak3_41r4?", //HTTP header API Key (basic security)
					"keyEnabled" => false,              //set if API key must be enabled
                ],
                "cli" => [
                    "loader" => ["models", "api/models"],
                    "core"   => ["checkout"]
                ]
            ],
            //App DI properties [$this->config->app->property]
            "app" => [
				//project properties
				"name"      => "Aira",   //App name
				"namespace" => "aira",   //App namespace (no usar underscore ni guiones)
				//crypto
				"cryptKey" => "4CCA1R4?",  //used for cypher
                //emails
                "emails" => [
                    "sender"  => "dev@crazycake.cl",
                    "support" => "dev@crazycake.cl",
                    "contact" => "contacto@crazycake.cl"
                ],
                //mandrill
                "mandrill" => [
                    "accessKey" => "RE3z78hfRD-qJm4EIlVnVw", //Mandrill API Key
                ],
				//amazon (Bucket is defined by environment)
				"aws" => [
					"accessKey" => "AKIAI6K4UWDZMKGOKV4Q",                      //AWS Access Key
					"secretKey" => "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",  //AWS Secret Key
					"s3Bucket"  => "aira",                                      //AWS S3 Bucket name
				],
				//google
				"google" => [
					"analyticsUA"  => "UA-48396138-2",                            //Analytics UA identifier (for frontend)
					"reCaptchaID"  => "6LcPEgATAAAAAFgS7BYStybvZ841I7VWt80rjy-2", //reCaptcha ID (frontend & backend)
					"reCaptchaKey" => "6LcPEgATAAAAANVUzYEvY_gTjW61LF4DhOIlJnyT", //reCaptcha Secret key (frontend & backend)
					"apiKey" 	   => "AIzaSyD-z7Wc0pJ_Clvgb9kzoBoQHLXU4F2fE4g",  //Google Maps Secret key (frontend & backend)
				]
            ]
        ];
    }
}
