/**
 * App.js
 * Browserify can only analyze static requires.
 * It is not in the scope of browserify to handle dynamic requires.
 */

//load bundle dependencies
require('webpack_core');

//UI framework
require('foundation');

//Google maps
require('google-maps');

//Google maps
require('chart.js');

var modules = [
    new (require('./src/ui.js'))(),
    new (require('./src/account.js'))(),
    new (require('./src/chart.js'))(),
    new (require('./src/map.js'))()
];

//set modules
core.setModules(modules);
