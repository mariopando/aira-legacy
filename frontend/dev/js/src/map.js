/**
 * Maps View Model
 * @class Maps
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "map";

    //++ View Model
    /*self.vm = {
        data : {
        },
        methods  : {},
    };*/

    //++ Properties
    self.amos            = {};
    self.startPos        = [-33.416366, -70.594704];
    self.map             = null;
    self.bounds          = null;
    self.positions       = null;
    self.speed           = 50; // km/h
    self.delay           = 50;
    self.infoWindow      = null; // tooltip instance
    self.divMapReference = $('#full-map'); // div map reference

    /**
     * Init function
     */
    self.init = function(data) {

        //self.amos = data.amos;
        self.createAmo();
        self.initGoogleMaps();
    };

    //++ Methods

    /**
     * Instantiate a map
     * @param google
     */
    self.createMap = function(google) {

        var position = new google.maps.LatLng(self.startPos[0], self.startPos[1]);

        var options = {
            center             : position,
            zoom               : 16,
            mapTypeId          : google.maps.MapTypeId.ROADMAP,
            mapTypeControl     : false,
            zoomControl        : true,
            zoomControlOptions : {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
        };

        self.map    = new google.maps.Map(self.divMapReference[0], options);
        self.bounds = new google.maps.LatLngBounds();
    };

    /**
     * Make a marker
     *
     * @param status_level
     * @returns {google.maps.MarkerImage}
     */
    self.getGoogleMarkerImage = function(status_level){

        var flags_images = ['marker.png','good-marker.png','alert-marker.png','danger-marker.png'];

        return new google.maps.MarkerImage(
            APP.baseUrl + 'images/icons/' + flags_images[status_level],
            null,
            new google.maps.Point(0, 0),
            new google.maps.Point(30, 30)
        );
    };

    /**
     * Load a marker for each amos object
     * @param google
     */
    self.assignMarkers = function(google) {

        _.forIn(self.amos, function(amos) {

            var position = new google.maps.LatLng(amos.position[0], amos.position[1]);

            self.bounds.extend(position);

            amos.marker = new google.maps.Marker({
                position : new google.maps.LatLng(amos.position[0],amos.position[1]),
                map      : self.map,
                title    : amos.id.toString(),
                icon     : self.getGoogleMarkerImage(amos.data.status_level)
            });

            // default listener, for amos animations
            google.maps.event.addListener(self.map, 'idle', function()
            {
                self.animateMarker(amos,
                    // The coordinates of each point you want the marker to go to.
                    // You don't need to specify the starting position again.
                    _.sampleSize(self.positions, 5)
                );
            });

            //make a tooltip infowindow object
            amos.marker.info = new google.maps.InfoWindow({
                //content: '<div class="tooltip">'+amos.id+
                content: '<div id="tooltip">'+
                            '<div class="status float-left">'+
                                '<div class="register level-'+amos.data.indicators.register.level+'">'+amos.data.indicators.register.counter+'</div>' +
                                '<div class="alert level-'+amos.data.indicators.alert.level+'">'+amos.data.indicators.alert.counter+'</div>' +
                                '<div class="alarm level-'+amos.data.indicators.alarm.level+'">'+amos.data.indicators.alarm.counter+'</div>' +
                            '</div>'+
                            '<div class="detail float-right">' +
                                '<h4>'+amos.data.name+'</h4>' +
                                '<p>'+amos.data.dni+'<br><br>' +
                                      amos.data.car_model+'<br>' +
                                      amos.data.car_patent+'</p>' +
                            '</div>'+
                        '</div>'
            });

            // tooltip listener
            google.maps.event.addListener(amos.marker, 'mouseover', function(){
                self.closeAllTooltips();
                amos.marker.info.open(self.map, amos.marker);
            });

            /*
             * The google.maps.event.addListener() event waits for
             * the creation of the infowindow HTML structure 'domready'
             * and before the opening of the infowindow defined styles
             * are applied.
             */
            google.maps.event.addListener(amos.marker.info, 'domready', function() {

                // Reference to the DIV which receives the contents of the infowindow using jQuery
                var iwOuter = $('.gm-style-iw');

                var iwBackground = iwOuter.prev();
                var iwCloseBtn   = iwOuter.next();

                // Remove outer elements
                iwBackground.css({'display' : 'none'});
                iwCloseBtn.css({'display' : 'none'});
            });

            ////load bounds
            self.map.fitBounds(self.bounds);
        });
    };

    /**
     * Close all position tooltips
     */
    self.closeAllTooltips = function() {

        _.forIn(self.amos, function(amos) {
            amos.marker.info.close();
        });
    };

    /**
     * Create a google map session
     */
    self.initGoogleMaps = function(){

        GoogleMapsLoader.KEY       = APP.googleApiKey;
        GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];

        GoogleMapsLoader.load(function(google) {
            self.createMap(google);
            self.assignMarkers(google);
        });
    };

    /**
     * Create amos collection
     */
    self.createAmo = function(){
        //positions reference
        self.positions =[
            [-33.41194144, -70.60362984],
            [-33.414995, -70.605979],
            [-33.417896, -70.600497],
            [-33.416366, -70.594704],
            [-33.413948, -70.602052],
            [-33.420306, -70.607041],
            [-33.421873, -70.614165],
            [-33.415463, -70.617355]];

        function Amo(id){
            this.id = id;
            this.position = null;
            this.marker = null;
            this.infoWindow = null;
            this.data = {
                name: 'Ricardo Maturana',
                dni: '7.987.336-9',
                car_model: 'Hyundai Accent',
                car_patent: 'DB·TR·64',
                status_level: _.random(0,3),
                indicators: {
                    register: {
                        level: 0,
                        counter: _.random(0,3)
                    },
                    alert: {
                        level: 2,
                        counter: _.random(3,5)
                    },
                    alarm: {
                        level: 3,
                        counter: _.random(5,9)
                    }
                }
            };
        }

        //create amos collection, 8 amos
        for(var a = 0; a < 8; a++) {

            self.amos[a] = new Amo(a);
            self.amos[a].position = self.positions[a];
        }
    };

    /**
     * simulate gps movement
     * @param marker
     * @param coords
     */
    self.animateMarker = function(amos, coords)
    {
        var target = 0;
        var km_h = self.speed || 50;
        coords.push([self.startPos[0], self.startPos[1]]);

        function goToPoint() {

            var lat  = amos.marker.position.lat();
            var lng  = amos.marker.position.lng();
            var step = (km_h * 1000 * self.delay) / 3600000; // in meters

            var dest = new google.maps.LatLng(
                coords[target][0], coords[target][1]);

            var distance =
                google.maps.geometry.spherical.computeDistanceBetween(
                    dest, amos.marker.position); // in meters

            var numStep  = distance / step;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            var i        = 0;

            function moveMarker() {

                lat += deltaLat;
                lng += deltaLng;
                i   += step;

                if (i < distance) {

                    amos.marker.setPosition(new google.maps.LatLng(lat, lng));
                    //self.map.setCenter(new google.maps.LatLng(lat, lng));
                    setTimeout(moveMarker, self.delay);
                }
                else {

                    amos.marker.setPosition(dest);
                    //self.map.setCenter(dest);
                    target++;

                    if (target == coords.length)
                        target = 0;

                    setTimeout(goToPoint, self.delay);
                }
            }

            moveMarker();
        }

        goToPoint();
    };
};
