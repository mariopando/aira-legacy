/**
 * Charts View Model
 * @class Contents
 */
module.exports = function() {
    //self reference
    var self        = this;
    self.moduleName = "chart";

    //++ View Model
    /*self.vm = {
        methods : {}
    };*/

    //++ Methods

    /**
     * Init function
     */
    self.init = function() {
        //app load UI
        self.activeDayChart();
    };

    /**
     * Active Day Chart
     */
    self.activeDayChart = function() {

        Chart.defaults.global.responsive     = true;
        Chart.defaults.global.scaleFontSize  = 14;
        Chart.defaults.global.scaleFontStyle = "bolder";

        if(!$('#day-tab').length)
            return;

        var day_chart = document.getElementById("day-canvas").getContext("2d");
        //var week_chart = document.getElementById("week-canvas").getContext("2d");
        //var year_chart = document.getElementById("year-canvas").getContext("2d");

        var gradient = day_chart.createLinearGradient(0, 0, 0, 400);
        gradient.addColorStop(0, 'rgba(225, 188, 40, 0.7)');
        gradient.addColorStop(0.5, 'rgba(225, 188, 40, 0)');

        var day = new Chart(day_chart).Line(self.dataDayChart(gradient), self.dataDayChartOptions());
        //var week = new Chart(week_chart).Line(self.dataDayChart(), self.dataDayChartOptions());
        //var year = new Chart(year_chart).Line(self.dataDayChart(), self.dataDayChartOptions());
    };

    self.dataDayChart = function(gradient) {

        return {
            labels: ["7:00","8:00","9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","24:00"],
            datasets: [{

                    label: "Speeding",
                    fillColor: gradient,
                    strokeColor: "#e1bc29",
                    pointColor: "#e1bc29",
                    pointStrokeColor: "#272729",
                    pointHighlightFill: "#e1bc29",
                    pointHighlightStroke: "#e1bc29",
                    pointLabelFontFamily : "'clearSansRegular'",
                    scaleLabel: "<%=value%kmh>",
                    scaleFontFamily: "'clearSansRegular','Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                    tooltipTitleFontFamily: "'clearSansRegular','Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                    tooltipFontFamily: "'clearSansRegular','Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                    data: [140, 120, 100, 120, 130, 140, 110, 80,120, 140, 150, 100, 110, 110, 120,130, 120, 80, 90]
                }]
        };
    };

    self.dataDayChartOptions = function() {

        return {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgb(151, 151, 151)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 0.2,

            // Boolean - If we want to override with a hard coded scale
            scaleOverride: true,

            // ** Required if scaleOverride is true **
            // Number - The number of steps in a hard coded scale
            scaleSteps: 10,

            // Number - The value jump in the hard coded scale
            scaleStepWidth: 7,

            // Number - The scale starting value
            scaleStartValue: 80,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve : true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.5,

            //Boolean - Whether to show a dot for each point
            pointDot : true,

            //Number - Radius of each point dot in pixels
            pointDotRadius : 9,

            // Boolean - Whether to show labels on the scale
            scaleShowLabels: true,

            // Interpolated JS string - can access value
            scaleLabel: "<%=value%> KM/H",

            // Interpolated JS string - can access value
            scaleArgLabel: "<%=value%> KM/H",

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 4,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 4,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //Tooltip values
            tooltipTemplate: "<%=label%>",

            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><%for(var i=0;i<datasets.length;i++){%><li><span class=\"<%=name.toLowerCase()%>-legend-marker\" style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%=datasets[i].label%></li><%}%></ul>"
        };
    };
};
