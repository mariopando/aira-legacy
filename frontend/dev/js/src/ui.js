/** ---------------------------------------------------------------------------------------------------------------
    UI Module
---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {

    //self context
    var self        = this;
    self.moduleName = "ui";

    //++ Properties
    self.map      = null;
    self.marker   = null;
    self.startPos = [42.42679066670903, -83.29210638999939];
    self.speed    = 50; // km/h
    self.delay    = 100;

    //++ UI Selectors
    _.assign(APP.UI, {
        //settings
        alert : {
            position    : "fixed",
            top         : "belowHeader",
            topForSmall : "0"
        },
        loading : {
            position    : "fixed",
            top         : "14%",
            topForSmall : "20%",
            center      : true
        },
        //selectors
        sel_header_profile : "#app-header-profile",
        sel_header_menu    : "#app-header-menu"
    });

    /**
     * Init function
     */
    self.init = function() {
        //app load UI
        self.uiSetUp();
    };

    /**
     * App Theme UI
     */
    self.uiSetUp = function() {

        //flow type setup
        /*$('body').flowtype({
            minimum   : 700,
            maximum   : 1200,
            minFont   : 12,
            maxFont   : 40,
            fontRatio : 30
        });*/

        //TODO: mover esta logica
        self.loadDriverMap();
        self.startTime();
    };

    /**
     * Add a digital clock on header
     */
    self.startTime = function(){

        if(!$('#nowtime').length) {
            return;
        }

        function timer() {

            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = formatTime(m);
            s = formatTime(s);

            $('#nowtime').html(h + ":" + m + ":" + s);
            var t = setTimeout(timer, 500);
        }

         // add zero in front of numbers < 10
        function formatTime(i) {

            if (i < 10) { i = "0" + i; }

            return i;
        }

        timer();
    };

    /**
     * Load Google Maps and Map Animation
     */
    self.loadDriverMap = function(){

        if(!$('#map').length)
            return;

        GoogleMapsLoader.KEY       = APP.googleApiKey;
        GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];

        GoogleMapsLoader.load(function(google) {

            var mapDiv = $('#map')[0];

            //auto height adjust
            self.autoResizeMapElement(mapDiv);

            //var crazycake = new google.maps.LatLng(-33.4118701,-70.6035554);
            var crazycake = new google.maps.LatLng(42.425175091823974, -83.2943058013916);
            var options = {
                center             : crazycake,
                mapTypeId          : google.maps.MapTypeId.ROADMAP,
                zoom               : 16,
                mapTypeControl     : false,
                zoomControl        : true,
                zoomControlOptions : {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
            };

            self.map = new google.maps.Map(mapDiv, options);

            var marker_image = new google.maps.MarkerImage(
                APP.baseUrl + 'images/icons/marker.png',
                null,
                new google.maps.Point(0, 0),
                new google.maps.Point(30, 30)
            );

            self.marker = new google.maps.Marker({
                position: new google.maps.LatLng(self.startPos[0], self.startPos[1]),
                map: self.map,
                icon: marker_image
            });

            // Add listener with animate markers
            google.maps.event.addListenerOnce(self.map, 'idle', function() {

                self.animateMarker(self.marker, [
                    // The coordinates of each point you want the marker to go to.
                    // You don't need to specify the starting position again.
                    [42.42666395645802, -83.29694509506226],
                    [42.42300508749226, -83.29679489135742],
                    [42.42304468678425, -83.29434871673584],
                    [42.424882066428424, -83.2944130897522],
                    [42.42495334300206, -83.29203128814697]
                ]);
            });
        });
    };

    /**
     * Autoresize map
     * @param mapDiv
     */
    self.autoResizeMapElement = function(mapDiv) {

        var div_to_resizing = $(mapDiv);
        var content_size    = 0;
        var container       = div_to_resizing.parent().parent();

        container.find('> .row').not('.driver-map-panel').map(function() {
            content_size = content_size + $(this).innerHeight();
        });

        $(window).resize(function() {
             div_to_resizing.css('height', container.height() - content_size);
        });

        $(window).trigger('resize');
    };

    /**
     * simulate gps movement
     * @param marker
     * @param coords
     */
    self.animateMarker = function(marker, coords)
    {
        var target = 0;
        var km_h = self.speed || 50;
        coords.push([self.startPos[0], self.startPos[1]]);

        function goToPoint() {

            var lat = marker.position.lat();
            var lng = marker.position.lng();
            var step = (km_h * 1000 * self.delay) / 3600000; // in meters

            var dest = new google.maps.LatLng(
                coords[target][0], coords[target][1]);

            var distance =
                google.maps.geometry.spherical.computeDistanceBetween(
                    dest, marker.position); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;

            function moveMarker() {

                lat += deltaLat;
                lng += deltaLng;
                i += step;

                if (i < distance) {

                    self.marker.setPosition(new google.maps.LatLng(lat, lng));
                    self.map.setCenter(new google.maps.LatLng(lat, lng));
                    setTimeout(moveMarker, self.delay);
                }
                else {

                    self.marker.setPosition(dest);
                    self.map.setCenter(dest);
                    target++;

                    if (target == coords.length)
                        target = 0;

                    setTimeout(goToPoint, self.delay);
                }
            }

            moveMarker();
        }

        goToPoint();
    };
};
