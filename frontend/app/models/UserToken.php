<?php

//imports
use CrazyCake\Account\BaseUserToken;

class UserToken extends BaseUserToken
{
    //this static method can be 'overrided' as late binding
    public static $TOKEN_EXPIRES_THRESHOLD = 3; //days

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who() {
        return __CLASS__;
    }
}
