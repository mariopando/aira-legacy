<?php
/**
 * Phalcon App Routes files
 * 404 error page is managed by Route 404 Plugin automatically
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

return function($router) {

	//Default route
	$router->add("/", [
	    'controller' => 'auth',
	    'action'     => 'signIn'
	]);

	//login
	$router->add("/signIn", [
	    'controller' => 'auth',
	    'action'     => 'signIn'
	]);

	//account
	$router->add("/account", [
		'controller' => 'account',
		'action'     => 'index'
	]);

	//driver
	$router->add("/drivers", [
		'controller' => 'driver',
		'action'     => 'index'
	]);

	//driver
	$router->add("/driver/:params", [
	    'controller' => 'driver',
	    'action'     => 'driver',
		'params' 	 => 1
	]);

	//speeding
	$router->add("/driver/speeding/:params", [
		'controller' => 'driver',
		'action'     => 'speeding',
		'params' 	 => 1
	]);

	//maps
	$router->add("/map", [
		'controller' => 'Gps',
		'action'     => 'index'
	]);

	$router->add("/report/:params", [
		'controller' => 'report',
		'action'     => 'index',
		'params' 	 => 1
	]);
};
