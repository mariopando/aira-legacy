<?php
/**
 * DriverController: handles Driver pages
 * @author Mario Pando <mario.pando@crazycake.cl>
 */

class DriverController extends SessionController
{
   	/**
	 * Initializer
	 */
	protected function initialize()
	{
		parent::initialize();

		//handle response, dispatch to auth/logout
		$this->_checkUserIsLoggedIn(true);
	}

	/**
	 * View - Index page
	 */
	public function indexAction()
	{
		//set current view
		$this->view->setVar("current_view", "drivers");

		//load js modules
		//$this->_loadJsModules([]);
	}


	/**
	* View - Profile page
	*/
	public function driverAction($id_driver = null, $type = null)
	{

	   $this->view->pick('driver/profile');
	}

	/**
	 * View - Speeding page
	 */
	public function speedingAction($id_driver = null) {

		$this->view->pick('driver/speeding');

		//load js modules
		$this->_loadJsModules([
			"chart" => null
		]);
	}
}
