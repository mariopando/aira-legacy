<?php
/**
 * IndexController, handles Index pages & send contact action
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class IndexController extends SessionController
{
    /**
     * View - Index page
     */
    public function indexAction()
    {
        //HTML props
        $this->view->setVar("html_metas", true);
    }
}
