<?php
/**
 * Report Controller, handles report pages
 * @author Mario Pando <mario.pando@crazycake.cl>
 */

class ReportController extends SessionController
{
    /**
	 * Initializer
	 */
	protected function initialize()
	{
		parent::initialize();

		//handle response, dispatch to auth/logout
		$this->_checkUserIsLoggedIn(true);
	}

    /**
     * Index Action
     * @param  string $report - The report name view
     */
    public function indexAction($report = null)
    {
        if(!is_string($report))
            $this->_redirectToNotFound();

        //$this->view->pick("report/$report");

        //load modules
        /*$this->_loadJsModules([

        ]);*/
    }
}
