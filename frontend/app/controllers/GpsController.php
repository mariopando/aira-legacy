<?php
/**
 * DriversController: handles Driver pages
 * @author Mario Pando <mario.pando@crazycake.cl>
 */

class GpsController extends SessionController
{

    /**
	 * Initializer
	 */
	protected function initialize()
	{
		parent::initialize();

		//handle response, dispatch to auth/logout
		$this->_checkUserIsLoggedIn(true);
	}

    /**
     * View - Index page
     */
    public function indexAction()
    {
        $this->view->pick('map/index');

        //set current view
        $this->view->setVar("current_view", "maps");

        //load js modules
        $this->_loadJsModules([
            "map" => null
        ]);
    }
}
