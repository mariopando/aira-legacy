<?php
/**
 * MailerController: sends email messages and notifications.
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Services\Mailer;

class MailerController extends CoreController
{
    /* traits */
    use Mailer;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //always call parent constructor
        parent::onConstruct();

        //set configurations
        $this->initMailer([
            "appName"     => $this->config->app->name,
            "client"      => $this->client,
            "mandrillKey" => $this->config->app->mandrill->accessKey,
            "cssFile"     => PUBLIC_PATH.'assets/mailing.min.css',
            //emails
            "senderEmail"  => $this->config->app->emails->sender,
            'supportEmail' => $this->config->app->emails->support,
            "contactEmail" => $this->config->app->emails->contact,
            //texts
            "trans" => [
                //messages subjects
                "subject_activation" => $this->trans->_("Confirma tu cuenta"),
                "subject_password"   => $this->trans->_("Recupera tu contraseña")
            ]
        ]);
    }

    /** ----------------------------------------------------- § ---------------------------------------------------- **/

    /**
     * View for debuging - Renders a mail message or a template
     */
    public function renderAction($view = null)
    {
        if(APP_ENVIRONMENT == 'production' || empty($view))
            $this->_redirectToNotFound();

        $user = User::getById($ticket->user_id);

        //set rendered view vars
        $this->mailer_conf["data_url"]  = $this->_baseUrl('fake/path');
        $this->mailer_conf["data_user"] = $user;

        //for contact template
        $this->mailer_conf["data_name"]    = "John Doe";
        $this->mailer_conf["data_email"]   = "john@doe.com";
        $this->mailer_conf["data_message"] = "Mail example for contact.";

        //get HTML
        $html_raw = $this->_getInlineStyledHtml($view, $this->mailer_conf);
        //render view
        die($html_raw);
    }
}
