<?php
/**
 * AccountController: controla todas las funciones de login, register, account-activation y logout
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake Traits
use CrazyCake\Account\AccountAuth;

class AuthController extends SessionController
{
    /* traits */
    use AccountAuth;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //always call parent constructor
        parent::onConstruct();
        //set configurations
        $this->initAccountAuth([
            "js_recaptcha" => false,
            //texts
            "trans" => [
                "auth_failed"        => $this->trans->_("El correo ó contraseña no son válidos."),
                "account_pending"    => $this->trans->_('Te hemos enviado un correo de activación. Haz click %a_open%aquí%a_close% si no has recibido este correo.',
                                            ["a_open" => '<a href="#">', "a_close" => '</a>']),
                "account_disabled"   => $this->trans->_('Esta cuenta se encuentra desactivada por incumplimiento a nuestros términos y condiciones, porfavor %a_open%comunícate aquí%a_close% con nuestro equipo.',
                                            ["a_open" => '<a href="javascript:core.redirectTo(\'contact\');"', "a_close" => '</a>']),
                "account_not_found"  => $this->trans->_('Esta cuenta no se encuentra registrada.'),
                "invalid_names"      => $this->trans->_('Tu nombre no parece ser válido.'),
                "recaptcha_failed"   => $this->trans->_('No hemos logrado verficar el reCaptcha, porfavor inténtalo de nuevo.'),
                "activation_success" => $this->trans->_('¡Tu cuenta ha sido activada!'),
                "activation_pending" => $this->trans->_('Te hemos enviado un correo a %email% para que actives tu cuenta.', ["email" => "<strong>{email}</strong>"]),
                //text titles
                "title_sign_in" => $this->trans->_("Ingresa a %app_name%", ["app_name" => $this->config->app->name]),
                "title_sign_up" => $this->trans->_("Regístrate en %app_name%", ["app_name" => $this->config->app->name])
            ]
        ]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Trait function after render view
     */
    protected function beforeRenderSignInView()
    {
        //load modules
        $this->_loadJsModules([
            "auth" => null
        ]);
    }

    /**
     * Trait function after render view
     */
    protected function beforeRenderSignUpView()
    {
        //load modules
        $this->_loadJsModules([
            "auth"  => null,
            "forms" => null
        ]);
    }
    /** ----------------------------------------------------- § ---------------------------------------------------- **/
}
