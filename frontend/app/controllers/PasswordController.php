<?php
/**
 * PasswordController: handles user account pass recovery
 * Traits has recovery & new actions
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Account\AccountPassRecovery;

class PasswordController extends SessionController
{
    /* traits */
    use AccountPassRecovery;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //always call parent constructor
        parent::onConstruct();
        //set configurations
        $this->initAccountPassRecovery([
            "javascript_modules" => [
                "passRecovery" => null
            ],
            //texts
            "trans" => [
                //texts
                "recaptcha_failed"  => $this->trans->_('No hemos logrado verficar el reCaptcha, porfavor inténtalo de nuevo.'),
                "account_not_found" => $this->trans->_('Esta cuenta no se encuentra registrada o no ha sido activada.'),
                "pass_mail_sent"    => $this->trans->_('Te hemos enviado un correo a %email% para recuperar tu contraseña.', ["email" => "<strong>{email}</strong>"]),
                "new_pass_saved"    => $this->trans->_('¡Tu contraseña ha sido modificada!'),
                //text titles
                "title_recovery"    => $this->trans->_("Recupera tu contraseña"),
                "title_create_pass" => $this->trans->_("Crea una nueva contraseña")
            ]
        ]);
    }

    /** ----------------------------------------------------- § ---------------------------------------------------- **/
}
