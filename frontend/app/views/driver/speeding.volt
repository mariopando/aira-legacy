{# Profile View #}
<div class="row speeding">
    <div class="driver-detail-panel">
        <div class="small-4 column">
            <div class="id">
                <img class="float-left" src="{{ static_url('../images/pics/profile-pic1.png') }}" alt="">
                <h2>Alex Cisternas</h2>
                <h3>54 años</h3>
                <h3>8.336.788-8</h3>
            </div>
        </div>
        <div class="small-4 column">
            <div class="car">
                <div class="row">
                    <div class="small-7 column">
                        <h2 class="small-">Pegeout Parner</h2>
                        <h3 class="float-left small-6">DG·58·96</h3>
                        <h3 class="float-right small-6">Diesel</h3>
                        <h3 class="float-left small-6">Año 2012</h3>
                        <h3 class="float-right small-6">1.6 c.c.</h3>
                    </div>
                    <img class="column small-5" src="{{ static_url('../images/pics/car-pic1.png') }}" alt="">
                </div>
            </div>
        </div>
        <div class="small-4 column">
            <div class="status">
                <div class="row">
                    <div class="small-12" id="status-text">online</div>
                    <div class="small-12"><h4>LAST UPDATE AT:  14:50, MAY 28</h4></div>
                </div>
            </div>
        </div>
    </div>

    <div class="driver-speeding-panel">
        <div class="small-11 float-left">
            <div class="small-10 float-left">
                    <h2 class="float-left">Speeding</h2>
                    <h4 class="float-right">Martes 21, Mar. 2016</h4>
            </div>
            <div class="small-2 float-right">
                <ul class="tabs" data-tabs id="chart-tabs">
                    <li class="tabs-title is-active"><a href="#day-tab" aria-selected="true">Day</a></li>
                    <li class="tabs-title"><a href="#week-tab">Week</a></li>
                    <li class="tabs-title"><a href="#year-tab">Year</a></li>
                </ul>
            </div>
            <div class="tabs-content small-12" data-tabs-content="chart-tabs">
                <div class="tabs-panel is-active" id="day-tab">
                    <canvas id="day-canvas" width="1600" height="250"></canvas>
                </div>
                <div class="tabs-panel" id="week-tab">
                    <canvas id="week-canvas" width="1600" height="250"></canvas>
                </div>
                <div class="tabs-panel" id="year-tab">
                    <canvas id="year-canvas" width="1600" height="250"></canvas>
                </div>
            </div>
        </div>
        <div class="small-1 float-right sidebar">
            <div class="small-12">
                Event<br> Log
            </div>
            <div class="small-12">
                <h1>50</h1>
                <h5>Alarma</h5>
            </div>
            <div class="small-12">
                <h1 class="alert">20</h1>
                <h5 class="alert">Alerta</h5>
            </div>
            <div class="small-12">
                <h1>04</h1>
                <h5>Registro</h5>
            </div>
        </div>
    </div>

    <div class="driver-timeline-panel">
        <div class="small-10 float-left">
            <h2 class="float-left">Timeline</h2>
            <h4 class="float-right">Martes 21, Mar. 2016</h4>
        </div>
        <div class="small-2 float-right">
            <ul class="tabs" data-tabs id="timeline-tabs">
                <li class="tabs-title is-active"><a href="#day-timeline-tab" aria-selected="true">Day</a></li>
                <li class="tabs-title"><a href="#week-timeline-tab">Week</a></li>
                <li class="tabs-title"><a href="#year-timeline-tab">Year</a></li>
            </ul>
        </div>
        <div class="tabs-content small-12" data-tabs-content="timeline-tabs">
            <div class="tabs-panel is-active" id="day-timeline-tab">
                <p class="alert">Alex Cisternas generó una <span class="alert">alerta</span> de exceso de velocidad a 80 km/h el día Miércoles 28 de Mayo a las 14:34:25</p>
                <p class="alert">Alex Cisternas generó una <span class="alert">alerta</span> de exceso de velocidad a 80 km/h el día Miércoles 28 de Mayo a las 14:34:25</p>
                <p class="verygood">Alex Cisternas generó una <span class="verygood">registro</span> de exceso de velocidad a 80 km/h el día Miércoles 28 de Mayo a las 14:34:25</p>
                <p class="danger">Alex Cisternas generó una <span class="danger">alarma</span> de exceso de velocidad a 80 km/h el día Miércoles 28 de Mayo a las 14:34:25</p>
                <p class="verygood">Alex Cisternas generó una <span class="verygood">registro</span> de exceso de velocidad a 80 km/h el día Miércoles 28 de Mayo a las 14:34:25</p>
                <p class="alert">Alex Cisternas generó una <span class="alert">alerta</span> de exceso de velocidad a 80 km/h el día Miércoles 28 de Mayo a las 14:34:25</p>
            </div>
            <div class="tabs-panel" id="week-timeline-tab">
            </div>
            <div class="tabs-panel" id="year-timeline-tab">
            </div>
        </div>
    </div>
</div>