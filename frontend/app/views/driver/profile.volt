{# Profile View #}
<div class="row profile">
    <div class="row driver-detail-panel">
        <div class="small-4 column">
            <div class="id">
                <img class="float-left" src="{{ static_url('../images/pics/profile-pic1.png') }}" alt="">
                <h2>Alex Cisternas</h2>
                <h3>54 años</h3>
                <h3>8.336.788-8</h3>
            </div>
        </div>
        <div class="small-4 column">
            <div class="car">
                <div class="row">
                    <div class="small-7 column">
                        <h2 class="small-">Pegeout Parner</h2>
                        <h3 class="float-left small-6">DG·58·96</h3>
                        <h3 class="float-right small-6">Diesel</h3>
                        <h3 class="float-left small-6">Año 2012</h3>
                        <h3 class="float-right small-6">1.6 c.c.</h3>
                    </div>
                    <img class="column small-5" src="{{ static_url('../images/pics/car-pic1.png') }}" alt="">
                </div>
            </div>
        </div>
        <div class="small-4 column">
            <div class="status">
                <div class="row">
                    <div class="small-12" id="status-text">online</div>
                    <div class="small-12"><h4>LAST UPDATE AT:  14:50, MAY 28</h4></div>
                </div>
            </div>
        </div>
    </div>

    {#Driver status#}
    <div class="row driver-status-panel">
        <a href="{{ url('driver/speeding') }}">
            <div class="small-3 columns">
                <div class="small-12 columns">
                    <div class="status">
                        <h3>Speeding</h3>
                        <h1><span>80</span> km/h</h1>
                        <div class="good progress">
                            <div class="progress-meter" style="width: 50%"></div>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="small-4 columns">
                        <h1 >04</h1>
                        <h5>Registro</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1 >20</h1>
                        <h5>Alerta</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1 >50</h1>
                        <h5>Alarma</h5>
                    </div>
                </div>
            </div>
        </a>
        <a href="#">
            <div class="small-3 columns">
                <div class="small-12 columns">
                    <div class="status">
                        <h3>Rpm</h3>
                        <h1><span>2000</span> km/h</h1>
                        <div class="danger progress">
                            <div class="progress-meter" style="width: 85%"></div>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="small-4 columns">
                        <h1>12</h1>
                        <h5>Registro</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1>69</h1>
                        <h5>Alerta</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1 class="danger">03</h1>
                        <h5>Alarma</h5>
                    </div>
                </div>
            </div>
        </a>
        <a href="#">
            <div class="small-3 columns">
                <div class="small-12 columns">
                    <div class="status">
                        <h3>Oil Temperature</h3>
                        <h1><span>30º</span>C</h1>
                        <div class="verygood progress">
                            <div class="progress-meter" style="width: 10%"></div>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="small-4 columns">
                        <h1 class="verygood">77</h1>
                        <h5>Registro</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1>08</h1>
                        <h5>Alerta</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1>09</h1>
                        <h5>Alarma</h5>
                    </div>
                </div>
            </div>
        </a>

        <a href="#">
            <div class="small-3 columns">
                <div class="small-12 columns">
                    <div class="status">
                        <h3>Motor Temperature</h3>
                        <h1><span>70º</span>C</h1>
                        <div class="alert progress">
                            <div class="progress-meter" style="width: 50%"></div>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="small-4 columns">
                        <h1>04</h1>
                        <h5>Registro</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1 class="alert">03</h1>
                        <h5>Alerta</h5>
                    </div>
                    <div class="small-4 columns">
                        <h1>07</h1>
                        <h5>Alarma</h5>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="row driver-map-panel">
        <div class="small-12" id="map"></div>
    </div>
</div>
