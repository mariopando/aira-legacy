{# Account Index #}
<div class="drivers-list">
    <ul class="tabs drivers-tabs" data-tabs>
        <li class="tabs-title is-active"><a href="#active-drivers" aria-selected="true">Activos (13)</a></li>
        <li class="tabs-title"><a href="#inactive-drivers">Inactivos (8)</a></li>
    </ul>
    <div class="tabs-content" data-tabs-content="drivers-tabs">
        <div class="active-drivers tabs-panel is-active">
            <div class="row">

                <a href="{{ url('driver') }}">
                    <div class="small-3 column">
                        <img src="{{ static_url('../images/pics/driver-pic1.jpg') }}" alt="">
                        <div class="row">
                            <div class="small-10 small-centered">
                                <p class="identification">Ricardo Maturana<span class="description">7.987.336-9</span></p>
                                <p class="vehicle_data">Hyundai Accent<span class="description">DB·TR·64</span> </p>
                            </div>
                        </div>
                    </div>
                </a>

                <a href="{{ url('driver') }}">
                    <div class="small-3 column">
                        <img src="{{ static_url('../images/pics/driver-pic2.jpg') }}" alt="">
                        <div class="row">
                            <div class="small-10 small-centered">
                                <p class="identification">Bernardo Fuentes<span class="description">15.838.480-9</span></p>
                                <p class="vehicle_data">Hyundai Accent<span class="description">DB·TR·65</span> </p>
                            </div>
                        </div>
                    </div>
                </a>

                <a href="{{ url('driver') }}">
                    <div class="small-3 column">
                        <img src="{{ static_url('../images/pics/driver-pic3.jpg') }}" alt="">
                        <div class="row">
                            <div class="small-10 small-centered">
                                <p class="identification">Pedro Juarez<span class="description">2.222.222-2</span></p>
                                <p class="vehicle_data">Hyundai Accent<span class="description">DB·TR·66</span> </p>
                            </div>
                        </div>
                    </div>
                </a>

                <a href="{{ url('driver') }}">
                    <div class="small-3 column">
                        <img src="{{ static_url('../images/pics/driver-pic4.jpg') }}" alt="">
                        <div class="row">
                            <div class="small-8 small-centered">
                                <p class="identification">Alberto Leiva<span class="description">9.865.485-5</span></p>
                                <p class="vehicle_data">Hyundai Accent<span class="description">DB·TR·67</span> </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="tabs-panel inactive-drivers">
            <p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
        </div>
    </div>
</div>
