{#  Drivers Parent Layout
	==============================================
#}
{# Drivers Wrapper #}
<div class="app-driver-wrapper app-container-wrapper">
    {# Header #}
    {{ partial("templates/header") }}
    {# content #}
    <div class="app-container app-drivers-content">
        {{ get_content() }}
    </div>
</div>