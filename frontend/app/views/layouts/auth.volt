{#  Auth Parent Layout
	==============================================
#}

{# Auth Wrapper #}
<div class="app-auth-wrapper">
    <div class="app-container app-login-content">
		{# SubHeader account (logo) #}
		{{ partial("templates/headerForm") }}
    	{# content #}
		{{ get_content() }}
	</div>
</div>
