{#  Account Parent Layout
	==============================================
#}
{# Account Wrapper #}
<div class="app-account-wrapper app-container-wrapper">
    {# Header #}
    {{ partial("templates/header") }}
    {# content #}
    <div class="app-container app-account-content">
        {{ get_content() }}
    </div>
</div>
