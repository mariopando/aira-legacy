{#  Drivers Parent Layout
	==============================================
#}
{# Drivers Wrapper #}
<div class="app-map-wrapper app-container-wrapper">
    {# Header #}
    {{ partial("templates/header") }}
    {# content #}
    <div class="app-container app-map-content">
        {{ get_content() }}
    </div>
</div>