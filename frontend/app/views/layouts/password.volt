{#  Password Recovery Parent Layout
	==============================================
#}

{# Auth Wrapper #}
<div class="app-auth-wrapper">
	<div class="app-container">
		{# SubHeader account #}
		{{ partial("templates/headerForm") }}
	    {# content #}
		{{ get_content() }}
	</div>
</div>