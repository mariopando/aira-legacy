{# SignIn View #}
<div id="vue-auth" class="app-sign-in-wrapper app-form">
    <img class="logo" src="{{ static_url('images/icons/iso.png') }}">
    {#<h6 class="subheader text-center">{{ trans._('Ingresa con tu correo.') }}</h6>#}

    {# Login Form #}
    <form data-validate v-on:submit="loginUserByEmail" action="javascript:void(0);">
        <div class="row large-collapse">
            <div class="columns small-12">
                <input name="email" type="email" size="50" maxlength="255" v-model="loginInputEmail" autocomplete="on" placeholder="{{ trans._('USUARIO') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa un usuario válido.') }}" />
            </div>
        </div>

        <div class="row large-collapse">
            <div class="columns small-12">
                <input name="pass" type="password" size="50" maxlength="20" autocomplete="off" placeholder="{{ trans._('CONTRASEÑA') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa tu contraseña.') }}" />
            </div>
        </div>

        {# submit button #}
        <div class="row large-collapse">
            <div class="columns small-12">
                <button type="submit" class="app-btn">
                    {#<img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />#}
                    <span>{{ trans._('Ingresar') }}</span>
                </button>
            </div>
        </div>
    </form>

    {# Account links #}
    <div class="links text-center">
        <h6 class="subheader">
            <a href="{{ url('password/recovery') }}" class="forgot-account">{{ trans._('¿OLVIDASTE TU CONTRASEÑA?') }}</a>
        </h6>
    </div>

</div>
