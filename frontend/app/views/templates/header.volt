{# HEADER #}
<header>
    <div class="top-bar">
        <div class="top-bar-title"><a href="{{ url() }}"></a></div>
        <div class="top-bar-left">
            <ul class="menu">
                {#<li class="menu-image">Aira</li>#}
                <li><?php echo \CrazyCake\Helpers\Dates::getTranslatedCurrentDate(); ?></li>
                <li id="nowtime"></li>
            </ul>
        </div>
        <div class="top-bar-right">
            <ul class="menu">
                <li class="{{ current_view == 'drivers' ? 'active' : 'default' }}"><a href="{{ url('drivers') }}">Flota</a></li>
                <li class="{{ current_view == 'maps' ? 'active' : 'default' }}"><a href="{{ url('map') }}">Mapa</a></li>
                <li><a href="{{ url('auth/logout') }}">Log out</a></li>
            </ul>
        </div>
    </div>
</header>
