Aira WebApp project
====================

Requisitos de entorno
---------------------

- Apache 2.4.x or greater HTTP Server
- Phalcon PHP 2.x framework. [Get Phalcon](https://phalconphp.com/es/download).
- PHP 5.6.x
- PHP extensions: `curl`, `mcrypt`, `gd`, `gettext` and `mysql`
- MySQL 5.6.x server (5.5 also works)
- Composer (Library dependencies, `packages/composer` folder)
- wkhtmltopdf (instalación abajo) [ref](http://wkhtmltopdf.org/)

Tasks
------

Ejecutar **_app.bash**
```
bash _app.bash
```

### Production bash aliases

```
alias socket_aira_start='bash $HOME/aira-webapp/sockets/_socket.bash -start -t'
alias socket_aira_stop='bash $HOME/aira-webapp/sockets/_socket.bash -stop -t'
alias socket_aira_restart='bash $HOME/aira-webapp/sockets/_socket.bash -restart -t'
```

API UDP Socket (NodeJs)
-----------------------

### Install

NodeJS version *0.10.x*

Instalar dependencias con `npm install` en este directorio.

### Execution

Node forever
```
forever start socket.js
```
Development or Testing Env mode:
```
node socket -d
node socket -t
```

Port definition:
```
node socket -p [port_number]
```

### Aliases for testing
```
alias aira_socket_start='bash $HOME/aira-webapp/sockets/_socket.bash -start -t'
alias aira_socket_stop='bash $HOME/aira-webapp/sockets/_socket.bash -stop -t'
alias aira_socket_restart='bash $HOME/aira-webapp/sockets/_socket.bash -restart -t'
```

API Testing
-----------

Para testear la API con **CURL**

local
```
curl -i --header "X-API-KEY: *Cr4ZyCak3_S0Nos?" -X GET http://localhost/aira-webapp/api/
```

testing
```
curl -i --header "X-API-KEY: *Cr4ZyCak3_S0Nos?" -X GET http://aira.api.m0.testing.crazycake.cl/
```

Database Migrations
-------------------

Phinx: [documentation](https://phinx.org/)
```
bash _app.bash phinx <commands>
```

Translations (GetText)
----------------------

Buscar nuevos translates en diferentes módulos
```
bash .tools/_translations.bash <module> -s
```

Compilar `.po` translation files en la carpeta `app` del módulo.
```
bash .tools/_translations.bash <module> -c
```

Para ver los 'locales' instalados en la instancia (getText)
```
locale -a
```

Composer
--------

Update Composer
```
php composer.phar self-update
```

Usage
```
php composer.phar install
```

Update Libraries
```
php composer.phar update
```

Update One Vendor
```
php composer.phar update foo/bar
```

Add Library
```
php composer.phar require "foo/bar:1.0.0"
```

Optimization loader for production
```
php composer.phar dump-autoload --optimize
```
