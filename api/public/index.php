<?php
/**
 * Index Phalcon File. PHP Settings must be set in php.ini (both files: Apache & CLI)
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//include App Loader
require dirname(dirname(__DIR__))."/app.php";

try {
    $app = new PhalconApp("api");
    $app->start();
}
catch (Exception $e) {
    echo $e->getMessage();
}
