<?php
/**
 * Analyse & process data, also outputs KPI reports.
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//libs
use CrazyCake\Helpers\GPS;
use CrazyCake\Helpers\Slug;

class WsAnalyserController extends WsCoreController
{
	/* consts */
	const DEFAULT_TIME_FREQ = 5; //frequency gather time, in segs. Seteado a 6 datos cada 30 segundos.
	const SPEED_TIME_FREQ 	= 3; //Seteado a 12 datos cada 30 segundos.

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Proccess new metrics
     * @access public
     * @param array $data Object with parsed data
     */
    public function processNewMetrics($data)
    {
    	$objects = array();

    	//loop through metrics remove malformed data
    	foreach ($data->metrics as $key => $values) {

    		$namespace = Slug::generate($key);
        	$date_time = clone $data->date_time;
    		$metrics   = $this->__getMetricObjects($data->amo_id, $date_time, $key, $namespace, $values);
    		//merge objects
    		$objects   = array_merge($objects, $metrics);
    	}

        return $objects;
    }

    /**
     * Parse a Batch of JSON Metric (validates that input json has a metric struct)
     * Example struct: [{ "a" : 1 }, { "n" : "rpm-test", "v" : "0.567" }, { "n" : "fuel-test", "v" : "15.8;18.9" }]
     * @access private
     * @param  string $payload
     * @return object data[amo_id, metrics, date_time]
     */
    public function parseMetricsBatchStruct($payload)
    {
        $valid_struct = true;
        $object_id    = 0;
        $date_time    = new DateTime('NOW');
        $metrics      = array(); //returns a formatted array, ready to save in db

        try {

            $json = json_decode($payload); //parse json as objects

            //if parsed data is a string means is an error
            if (json_last_error() != JSON_ERROR_NONE)
                throw new Exception("Invalid JSON format.");

            if (!is_array($json))
                throw new Exception('Invalid JSON struct format, array expected.');

            //loop throught json objects in array
            for ($i = 0; $i < count($json); $i++) {
                //get objects
                $object = $json[$i];
                //1st element (amo_id)
                if ($i == 0) {

                    if (!isset($object->a) || !is_numeric($object->a))
                        throw new Exception('Invalid Object in array, amo struct expected. Example: {"a":1}');

                    $object_id = $object->a; //set amo id
                    continue;
                }

                //other elements
                if (!isset($object->n) || !isset($object->v))
                    throw new Exception('Invalid Object in array, metric struct expected. Example: {"n":"name","v":"value1;value2"}');

                //detect if object is a time metric
                if (strtolower($object->n) == Metric::TIME_NAMESPACE) {
                    //parse date
                    if(!$date_time = $this->__parseMetricsBatchDate($object->v))
                        throw new Exception('Invalid Time Object format, example format: 19/4/15;18:8:34');

                    continue;
                }

                //set key -> value
                $metrics[$object->n] = $object->v;
            }
        }
        catch (Exception $e) {
            return $e->getMessage();
        }

        //return a array of metrics object arrays
        $data = new \stdClass();
        $data->amo_id    = $object_id;
        $data->metrics   = $metrics;
        $data->date_time = $date_time;

        return $data;
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * get metric object for ORM
     * @param  int $amo_id
     * @param  DateTime $date_time Got from hardware device
     * @param  string $name
     * @param  string $namespace
     * @param  string $values
     * @return object (Metric)
     */
    private function __getMetricObjects($amo_id, $date_time, $name, $namespace, $values)
    {
        $values = explode(";", $values);
        //check for empty values
        if(empty($values))
            return false;

        $objects   = array();
        //get frequency and count: para la velocidad el último dato tiene el mismo timestamp, para los demas se resta un ciclo.
        $frequency = self::DEFAULT_TIME_FREQ;
        $count     = count($values) - 1;

        //special case for speed
        if($namespace == Metric::SPEED_NAMESPACE) {
            $frequency = self::SPEED_TIME_FREQ;
            $count++;
        }

        //get time substraction
        $time_sub = $count * $frequency;
        //substract time proportional of items count
        date_sub($date_time, date_interval_create_from_date_string($time_sub . ' seconds'));
        //print_r("  ".$time_sub." -> ".count($values)." ".$date_time->format("Y-m-d H:i:s")." ");

        foreach ($values as $k => $v) {

            //discard empty data
            if($v == '')
                continue;

            $metric = new \stdClass();
            $metric->amo_id    = $amo_id;
            $metric->name      = $name;
            $metric->namespace = $namespace;
            $metric->value     = $v;

            //add time (acumulative for object)
            date_add($date_time, date_interval_create_from_date_string(($k == 0 ? 0 : $frequency) . ' seconds'));
            //set device time
            $metric->device_time = $date_time->format("Y-m-d H:i:s");

            //check special cases (this must be below time add)
            if(!$this->__validatesCoordinateMetric($metric))
            	continue;

            //push value to array
            array_push($objects, $metric);
        }

        return $objects;
    }

    /**
     * Check for a valid coordinate and update its value
     * @param object $metric
     */
    private function __validatesCoordinateMetric(&$metric)
    {
    	//format coordinates
		if($metric->namespace !== Metric::COORDINATE_NAMESPACE)
			return true;

		try {
			//check format
			$coords = explode(",", $metric->value);
			if(count($coords) != 2)
				throw new Exception("Invalid coordinate");

			$new_coord = GPS::parseDegreesMinutesCoordinate($coords[0], $coords[1]);
			$metric->value = implode(",", $new_coord);
			return true;
		}
		catch (Exception $e) {
    		return false;
		}
	}

    /**
     * Parse date object value, input format: d/m/Y;H:i:s (example: 19/4/15;18:8:34), output DateTime object
     * @access private
     * @param  string $value
     * @return object (DateTime)
     */
    private function __parseMetricsBatchDate($value)
    {
        $value = trim($value);

        //format date
        try {
            $format     = explode(";", $value);
            $time_array = explode(":", $format[1]);
            $time_str   = sprintf("%02d:%02d:%02d", $time_array[0], $time_array[1], $time_array[2]);
            $value      = $format[0]." ".$time_str;

            $date_time = DateTime::createFromFormat('j/n/y G:i:s', $value); //no leading zeros
            return $date_time;
        }
        catch (Exception $e) {
            return false;
        }
    }
}
