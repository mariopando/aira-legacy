<?php
/**
 * WsMetricController: metrics services
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class WsMetricController extends WsCoreController
{
    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();

        //extended error codes
        $this->CODES['3001'] = "Error creating Excel file";
        $this->CODES['3002'] = "No output to show in Excel file";
        $this->CODES['3004'] = 'Invalid JSON batch struct.';
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Save a new  bactch of metric (POST)
     * @access public
     * @return string json
     */
    public function newBatch()
    {
        //validate and filter request params data, second params are the required fields
        $data = $this->_handleRequestParams([
            "payload" => "" //do not sanitize data
        ]);

        $analyser = new WsAnalyserController();
        //parse data
        $parsed_data = $analyser->parseMetricsBatchStruct($data['payload']);

        //validates data
        if(is_string($parsed_data)) {
            $this->CODES['3004'] .= " Description: $parsed_data";
            $this->_sendJsonResponse(3004);
        }

        //process new metrics
        $metrics = $analyser->processNewMetrics($parsed_data);
        //save data
        $saved_batch = Metric::saveBatch($metrics);
        //set payload
        $payload = $saved_batch;
        //response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * GET metrics by filters: 'date', 'namespace', results objects limited by 'number' & 'offset'.
     * If date has the 'last' value, the api returns result ordered by date DESC
     * @access public
     * @return string json
     */
    public function getMetrics()
    {
        //validate and filter request params data, second params are the required fields
        $data = $this->_handleRequestParams([
            'amo_id'     => 'int',
            '@date'      => 'string',
            '@namespace' => 'string',
            '@number'    => 'int',
            '@offset'    => 'int',
            '@distinct'  => 'int',
            '@kpis'      => 'int'
        ],'GET');
        //var_dump($data);exit;

        //set limits
        $data['limits'] = $this->_handleNumberAndOffsetParams($data['number'], $data['offset'], Metric::DEFAULT_SEARCHS);

        //$this->_logDatabaseStatements();

        //get dynamic metrics
        $metrics = Metric::getMetricsByData($data);

        //get kpi metrics and merge them?
        if(!empty($data['kpis']))
            $metrics = array_merge($metrics, Kpi::getMetrics($data));

        //set payload
        $payload = $metrics;
        //send response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * Get metrics distinct namespaces
     * @access public
     * @return string json
     */
    public function getNamespaces()
    {
        $payload = Metric::getDistinctNamespaces();

        //send response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * Dump metrics to an downloadable excel file (GET)
     * @access public
     * @return string json
     */
    public function dumpMetrics()
    {
        //validate and filter request params data, second params are the required fields
        $data = $this->_handleRequestParams([
            '@date'      => 'string',
            '@namespace' => 'string',
            '@url'       => 'int',
        ],'GET');

        //set search conditions
        $conditions = "";
        $binds = [];

        //date
        if (!is_null($data['date'])) {
            $conditions = "created_at >= :created_at:";
            $binds["created_at"] = $data['date'];
        }

        //namespace
        if (!is_null($data['namespace'])) {
            $conditions .= empty($conditions) ? "" : " AND ";
            $conditions .= "namespace = :namespace:";
            $binds["namespace"] = $data['namespace'];
        }

        //get model data
        $metrics = Metric::find([$conditions, "bind" => $binds]);

        //no results? no output to show
        if (empty($metrics->count()))
            $this->_sendJsonResponse(3002);

        //set payload
        $payload = $metrics->toArray();

        //create the output excel file
        $dump   = new DumpController();
        $fields = array('id', 'namespace', 'value', 'created_at');
        $file   = $dump->createExcelFile($payload, $fields);

        if (!$file)
            $this->_sendJsonResponse(3001);

        //send URL or file attachment
        if (!is_null($data['url']) && $data['url'] == true) {
            // response
            $filepath = explode("/", $file);
            $url      = APP_BASE_URL . "uploads/sheets/" . end($filepath);
            $this->_sendJsonResponse(200, array("url" => $url));
        }
        else {
            //set MIME type
            $this->response->setContentType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->response->setFileToSend($file);
            $this->response->send();
        }
        return;
    }
}
