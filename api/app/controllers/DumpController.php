<?php
/**
 * DumpController: manage data dumps
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//PhpExcel
use \PHPExcel;

class DumpController extends WsCoreController
{
    /**
     * @var string
     */
    private $upload_dir;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
       //set directory and create if not exists
        $this->upload_dir = PUBLIC_PATH."uploads/sheets/";

        if(!is_dir($this->upload_dir))
            mkdir($this->upload_dir, 0775);

        //PHP configs: force memory limit for heavy processing
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 180); //3 mins
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Creates an Excel File
     * @access public
     * @param  array  $payload metrics data
     * @param  array  $fields  array of column names
     * @return string The saved file path
     */
    public function createExcelFile($payload = array(), $fields = array())
    {
        if (empty($payload) || empty($fields))
            throw new Exception('DumpController::createExcelFile -> Payload and fields must be a non-empty array');

        //Create new PHPExcel object
        $excelSheetObj = new \PHPExcel();

        //Set properties
        $excelSheetObj->getProperties()->setCreator("CrazyCake Technologies");
        $excelSheetObj->getProperties()->setLastModifiedBy("Nicolas Pulido");
        $excelSheetObj->getProperties()->setTitle($this->config->app->name." Dump - Office 2007");
        $excelSheetObj->getProperties()->setSubject($this->config->app->name." Dump (".date('d/m/Y').")");
        $excelSheetObj->getProperties()->setDescription("Testing dump of DB.");

        //set active sheet
        $excelSheetObj->setActiveSheetIndex(0);

        //Add object properties to 1st row data
        $alphas    = range('A', 'Z');
        $cell_cols = array();

        for ($i = 0; $i < count($fields); $i++) {

            if (!isset($alphas[$i]))
                break;

            $colname = $alphas[$i];
            $prop    = $fields[$i];
            //write to document
            $excelSheetObj->getActiveSheet()->SetCellValue($colname . "1", $prop);

            //set cell range data type
            if (isset($payload[1])) {

                $obj = $payload[1];
                //is a numeric value
                if (isset($obj[$prop]) && is_numeric($obj[$prop])) {
                    $excelSheetObj->getActiveSheet()->getStyle($colname.'2:'.$colname.(count($payload) + 1))
                    ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
                }
            }

            //set style format
            $excelSheetObj->getActiveSheet()->getStyle($colname . "1")->applyFromArray([
                'fill' => [
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => '0077CC']
                ],
                'font' => [
                    'color' => ['rgb' => 'FFFFFF'],
                    'size'  => 12,
                    'bold'  => true
                ]
            ]);

            //set width
            $excelSheetObj->getActiveSheet()->getColumnDimensionByColumn($colname)->setAutoSize(false);
            $excelSheetObj->getActiveSheet()->getColumnDimension($colname)->setWidth(20);

            //set data style
            $excelSheetObj->getActiveSheet()->getStyle($colname.'2:'.$colname.(count($payload) + 1))->applyFromArray([
                'font' => ['size' => 11]
            ]);

            //save col name
            array_push($cell_cols, $colname);
        }

        //copy payload, loop through payload
        $j = 2; //row
        foreach ($payload as $object) {

            $i = 0; //col

            foreach ($fields as $prop) {

                if (isset($object[$prop]))
                    $excelSheetObj->getActiveSheet()->SetCellValue($cell_cols[$i] . $j, $object[$prop]);

                $i++;
            }

            $j++;
        }

        //rename sheet
        $excelSheetObj->getActiveSheet()->setTitle($this->config->app->name.' 01');
        //save Excel 2007 file
        $excelWriterObj = new PHPExcel_Writer_Excel2007($excelSheetObj);
        $savedFilePath  = $this->upload_dir.$this->config->app->name."_".date('d_m_Y')."_".uniqid().".xlsx";

        //create file!
        try {
            $excelWriterObj->save($savedFilePath);
        }
        catch (Exception $e) {
            throw $e;
        }

        //return saved filepath
        return $savedFilePath;
    }
}
