<?php
/**
 * UploadController: uploads data as WebService
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Helpers\Slug;

class WsUploadController extends WsCoreController
{
    /* consts */
    const FILE_MAX_SIZE_ALLOWED = 5120; //KB
    const FILE_TYPES_ALLOWED    = ["txt", "csv"]; //allowed file types

    /**
     * upload directory
     * @var string
     */
    private $upload_dir;

	/**
     * Construct Event
     */
	protected function onConstruct()
	{
        //call parent construct 1st
        parent::onConstruct();

        //set directory and create if not exists
        $this->upload_dir = PUBLIC_PATH."uploads/files/";

        if(!is_dir($this->upload_dir))
            mkdir($this->upload_dir, 0775);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Upload a csv or txt file (POST)
     * @return string json
     */
    public function uploadFile()
    {
        $uploaded_files = [];
        $errors         = [];

        // Check if the user has uploaded files
        if ($this->request->hasFiles() == true) {
            // loop through uploaded files
            foreach ($this->request->getUploadedFiles() as $file) {

                $validated_file = $this->_validateUploadedFile($file);

                //check for error
                if( $validated_file['error'] ) {
                    array_push($errors, $validated_file['exception']);
                }
                else {
                    $file_details = $validated_file['name']." -> ".$validated_file['size']." KB. (".$validated_file['key'].",". $validated_file['ext'].")";
                    array_push($uploaded_files, $file_details);

                    //Move the file into the application
                    $file->moveTo($this->upload_dir . $validated_file['namespace']);
                }
            }
        }

        //no files?
        if(empty($uploaded_files) && empty($errors))
            $this->_sendJsonResponse("901");

        //set payload
    	$payload = [
            "uploaded" => $uploaded_files,
            "errors"   => $errors
        ];

		//response
		$this->_sendJsonResponse("200", $payload);
    }

    /**
     * Validate uploaded file
     * @return string json
     */
    private function _validateUploadedFile($file)
    {
        //get file properties
        $file_key        = $file->getKey();
        $file_name       = $file->getName();
        $file_name_array = explode(".", $file_name);

        $file_ext       = end( $file_name_array ); //get file extension
        $file_cname     = str_replace(".$file_ext", "", $file_name);  //get clean name
        $file_mimetype  = $file->getRealType();                       //real file MIME type
        $file_size      = (float)($file->getSize() / 1000);           //set to KB unit
        $file_path      = $file->getRealPath();                       //get real temp path
        $file_namespace = Slug::generate($file_cname);                //clean name to namespace with slug lib

        //set array keys
        $validated_file = [
            'name'      => $file_name,
            'namespace' => $file_namespace.".$file_ext",
            'size'      => $file_size,
            'key'       => $file_key,
            'ext'       => $file_ext,
            'mime'      => $file_mimetype,
            'error'     => false,
            'exception' => false
        ];
        //var_dump($validated_file);exit;

        //check size
        if($file_size > self::FILE_MAX_SIZE_ALLOWED) {
            $validated_file['error']     = true;
            $validated_file['exception'] = "the uploaded file \"".$file_name."\" exceeds the maximum size allowed (".self::FILE_MAX_SIZE_ALLOWED." KB.)";
        }
        //check extension
        else if(!in_array($file_ext, self::FILE_TYPES_ALLOWED)) {
            $validated_file['error']     = true;
            $validated_file['exception'] = "the filetype you are attempting to upload is not allowed. (".$file_name." -> ".$file_key.")";
        }

        return $validated_file;
    }
}
