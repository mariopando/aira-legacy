<?php
/**
 * WsCoreController: API for user object
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake
use CrazyCake\Core\WsCore;

class WsCoreController extends WsCore
{
    /** const */
    const HEADER_DATE_NAME = 'X-DATE';

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();

         //set default header
        $this->response->setHeader(self::HEADER_DATE_NAME, date('Y-m-d H:i:s'));
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Welcome API
     * @access public
     */
    public function welcome()
    {
        $payload = "Welcome to CrazyCake ".$this->config->app->name." API (v. ".$this->version.")";

        $this->_sendJsonResponse(200, $payload);
    }
}
