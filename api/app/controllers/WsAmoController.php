<?php
/**
 * WsAmoController: Abstract Mobile Object Controller
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Helpers\GPS;

class WsAmoController extends WsCoreController
{
    /* consts */
    const AMOS_SEARCH_NUMBER             = 300;
    const AMO_DEFAULT_DISTANCE_THRESHOLD = 25000; //meters

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Save a new amo (POST)
     * @access public
     * @return string json
     */
    public function newAmo()
    {
        //validate and filter request params data, second params are the required fields
        $data = $this->_handleRequestParams([
            'name' => 'string',
            'type' => 'string'
        ]);

        //save data to db
        $amo = new Amo();

        if (!$amo->save($data))
            $this->_sendJsonResponse(null, $amo->parseOrmMessages());

        //unset some data
        unset($amo->created_at);
        //set payload
        $payload = $amo;
        //response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * GET amos by GPS location (optional): 'latitude', 'longitude', 'type'
     * @access public
     * @return string json
     */
    public function getAmo()
    {
        //validate and filter request params data, second params are the required fields
        $data = $this->_handleRequestParams([
            'latitude'  => 'float',
            'longitude' => 'float',
            'range'     => 'float',
            '@type'     => 'string',
            '@number'   => 'int',
            '@offset'   => 'int'
        ],'GET');

        //set search conditions
        $conditions = [];

        //set number and offset
        $limit = $this->_handleNumberAndOffsetParams($data['number'], $data['offset'], self::AMOS_SEARCH_NUMBER);
        //print_r($limit);exit;

        //type
        if (!is_null($data['type']))
            array_push($conditions, "type = '" . $data['type'] . "'");

        //join conditions (AND)
        $conditions = implode(" AND ", $conditions);

        //get model data
        $amos = Amo::getByConditions($conditions, $limit);
        $amos_array = [];

        //set distance threshold
        $distance_threshold = self::AMO_DEFAULT_DISTANCE_THRESHOLD;

        if(is_numeric($data['range']) && $data['range'] >= 50)
            $distance_threshold = $data['range'];

        //filter neatest AMOs
        foreach ($amos as $amo) {
            //calculate distance
            $amo->distance = GPS::vincentyGreatCircleDistance($amo->latitude, $amo->longitude, $data['latitude'], $data['longitude']);
            //exclude object if is too far away (threshold)
            if ($amo->distance <= $distance_threshold)
                array_push($amos_array, $amo);
        }

        //set payload
        $payload = $amos_array;
        //send response
        $this->_sendJsonResponse(200, $payload);
    }
}
