<?php

use Phalcon\Mvc\Model\Message;
//other imports
use CrazyCake\Models\Base;

class Metric extends Base
{
    /* consts */
    const TIME_NAMESPACE         = 'tiempo';
    const SPEED_NAMESPACE        = 'velocidad';
    const ACCEL_NAMESPACE        = 'aceleracion';
    const RPM_NAMESPACE          = 'rpm';
    const ENGINE_TIME_NAMESPACE  = 'tpo-encendido';
    const COORDINATE_NAMESPACE   = 'coord';
    const LATITUDE_NAMESPACE     = 'latitud';
    const LONGITUDE_NAMESPACE    = 'longitud';
    //search defaults
    const DEFAULT_SEARCHS = 100;

    /**
     * @var int
     */
    public $amo_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $namespace;

    /**
     * @var string
     */
    public $value;

    /**
     * real device time gathered from hardware
     * @var string
     */
    public $device_time;

    /**
     * timestamp value
     * @var string
     */
    public $created_at;

    /* arbitrary props */

    /**
     * KPI's namespaces
     * cdt: Continuous Driving Time, suma de tiempo encendido contando desde el último '0' durante el día.
     * ddt: Daily Driving Time, suma acumulativa de tiempo encendido durante el día.
     * drt: Daily Rest Time, suma de tiempo encendido del ultimo '0' al no-último 0 durante el día
     * night: Night Driving, suma de tiempo encendido a partir de las 19 hrs durante día.
     * wdt: Weekly Driving Time, suma semanal acumulada de ddt.
     * wrt: Weekly Rest Time, suma semanal acumulada de drt.
     * h_a: Harsh Accelerations, aceleraciones bruscas
     * h_b: Harsh Breakings, frenadas bruscas
     * @var array
     * @static
     */
    private static $kpis = ['cdt', 'ddt', 'drt', 'night', 'h_a', 'h_b', 'wdt', 'wrt'];

    /**
     * Initializer
     */
    public function initialize()
    {
        //Skips fields/columns on both INSERT/UPDATE operations
        $this->skipAttributes(['created_at']);

        //model relations (null foreign keys must be set manually in beforeSave hook)
        $this->belongsTo("amo_id", "Amo", "id");
    }

    /**
     * Before Save Event
     */
    public function beforeSave()
    {
        //validation for Foreign Keys that accepts Nulls values
        if (!is_null($this->amo_id)) {

            if (!Amo::countById($this->amo_id)) {
                $this->appendMessage(new Message("given amo_id don't exists"));
                return false;
            }
        }

        //check device time
        if(is_null($this->device_time))
            $this->device_time = date("Y-m-d H:i:s");

        return true;
    }

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * Get Metric by data
     * @param  array $data The input received data
     * @return array
     */
    public static function getMetricsByData($data = array())
    {
        //set search conditions
        $conditions = [];
        //set order
        $order_by = "id";

        //for distinct just select first 20 results
        if (!empty($data['distinct']))
            $data['offset'] = self::DEFAULT_SEARCHS * 0.4;

        //set number and offset
        $find_limits = isset($data['limits']) ? $data['limits'] : null;
        //print_r($find_limits);exit;

        //fix date value
        if(isset($data['fixed_date']) && $data['fixed_date']) {
            //set condition
            array_push($conditions, "DATE(device_time) = '".$data['date']."'");
            $order_by = "device_time ASC";
        }
        else {
        //date (check the last value)
             if(!is_null($data['date'])) {

                if ($data['date'] != "last")
                    array_push($conditions, "device_time >= '".$data['date']."'");
                else
                    $order_by = "device_time DESC";
            }
        }

        //amo id
        if (!is_null($data['amo_id']))
            array_push($conditions, "amo_id = '".(int)$data['amo_id']."'");

        //namespace
        if (!is_null($data['namespace']))
            array_push($conditions, "namespace = '".$data['namespace']."'");

        //join conditions (AND)
        $conditions = implode(" AND ", $conditions);

        $statements = [
            "columns" => "id, amo_id, namespace, value, device_time",
            "order"   => $order_by
        ];

        if(!empty($conditions))
            $statements["conditions"] = $conditions;

        if(!is_null($find_limits))
            $statements["limit"] = $find_limits;

        //get model data
        $metrics = self::find($statements);

        //distinct namespaces?
        if (!empty($data['distinct']))
            return $metrics->toArray();

        //set white list
        $white_list  = [];
        $new_metrics = [];

        foreach ($metrics as $obj) {

            //save object & namespace in whitelist?
            if(in_array($obj->namespace, $white_list))
                continue;

            //push to array
            array_push($white_list, $obj->namespace);
            array_push($new_metrics, $obj);
        }

        //update metrics array
        return $new_metrics;
    }

    /**
     * Save batch of metrics, input is a array of metrics (json decoded)
     * @param  array $data
     * @return array
     */
    public static function saveBatch($data)
    {
        $namespaces = [];
        $saved      = 0;
        $errors     = 0;

        //loop throught data
        foreach ($data as $object) {

            //push distinct namespaces
            if(!in_array($object->namespace, $namespaces))
                array_push($namespaces, $object->namespace);

            //save data to db
            $metric = new Metric();

            if($metric->save((array)$object))
                $saved++;
            else
                $errors++;
        }

        return [
            "saved_count"  => $saved,
            "errors_count" => $errors,
            "message"      => ($errors > 0 ? "amo_id don't exists" : "data saved ok"),
            "namespaces"   => $namespaces
        ];
    }

    /**
     * Get distinc metrics filtered by namespaces
     * @return array
     */
    public static function getDistinctNamespaces()
    {
        $result = self::getByQuery("SELECT DISTINCT(namespace) FROM metric");

        if(!$result)
            return [];

        $namespaces = [];
        foreach ($result as $obj)
            array_push($namespaces, $obj->namespace);

        return $namespaces;
    }
}
