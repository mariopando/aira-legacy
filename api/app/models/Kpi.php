<?php

use Phalcon\Mvc\Model\Message;
//other imports
use CrazyCake\Models\Base;
use CrazyCake\Helpers\Dates;

class Kpi extends Base
{
    /* const */
    const DEFAULT_TIME_FORMAT = 'Y-m-d H:i:s';
    const CURDATE_FORMAT      = 'Y-m-d';   //permite debugear fecha estatica, ej:Y-m-08.
    const DAY_ZERO_HOUR       = '00:00:00';
    const DAY_LAST_HOUR       = '23:59:59';
    const DAY_OPENING_HOUR    = '05:00:00';
    const DAY_CLOSING_HOUR    = '19:59:59';
    const DAY_WORKED_TIME     = 13 * 60 * 60; //total seconds in a working day (horario dia)
    const NIGHT_OPENING_HOUR  = '20:00:00';

    /**
     * KPI's namespaces
     * cdt: Continuous Driving Time, tiempo continuo de manejo.
     * ddt: Daily Driving Time, acumulativo de tiempo encendido en horario diurno en el día.
     * drt: Daily Rest Time, tiempo total de descanso en horario diurno, acumulado día.
     * night: Night Driving, suma de tiempo encendido horario nocturno.
     * wdt: Weekly Driving Time, suma semanal acumulada de ddt.
     * wrt: Weekly Rest Time, suma semanal acumulada de drt.
     * @var array
     * @static
     */
    private static $NAMESPACES = [
        'cdt'   => 'getContinuousDrivingTime',
        'ddt'   => 'getDailyDrivingTime',
        'drt'   => 'getDailyRestTime',
        'night' => 'getNightDrivingTime',
        'h_a'   => 'getHarshAccelerations',
        'h_b'   => 'getHarshBreakings'
    ];

    /** ------------------------------------------- § ----------------------------------------------- **/

    /**
     * Late static binding
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string The current class name
     */
    public static function who()
    {
        return __CLASS__;
    }

    /**
     * Get KPI metrics from database
     * @param  array $data The input received data
     * @return array of metrics
     */
    public static function getMetrics($data = array())
    {
        //set new array
        $metrics = [];
        $i       = 1;
        //for each kpi
        foreach (self::$NAMESPACES as $namespace => $fn) {
            //creates new metric object
            $metric = new \stdClass();

            $metric->id          = (string)$i++;
            $metric->amo_id      = (int)$data['amo_id'];
            $metric->namespace   = $namespace;
            $metric->device_time = date(self::DEFAULT_TIME_FORMAT);
            //set metric value by calling defined function
            $metric->value = self::$fn($data['amo_id']);

            //namespace filter
            if (is_null($data['namespace'])) {
                //push object to array
                array_push($metrics, $metric);
            }
            else {
                if($data['namespace'] == $metric->namespace)
                    array_push($metrics, $metric);
            }
        }

        return $metrics;
    }

    /**
     * Continuous Driving Time, it's a real time metric in a work day
     * @param  int $amo_id
     * @return string The formatted output
     */
    public static function getContinuousDrivingTime($amo_id = 0)
    {
        $namespace = Metric::ENGINE_TIME_NAMESPACE;
        $cur_date  = date(self::CURDATE_FORMAT)." ".self::DAY_ZERO_HOUR;

        //get last zero engine time in given date
        $sql = " SELECT id
                 FROM metric
                 WHERE amo_id = $amo_id
                    AND namespace = '$namespace'
                    AND value = '0'
                    AND device_time >= '$cur_date'
                 ORDER BY id DESC
                 LIMIT 1 ";

        //get target id
        $target_id = Metric::getPropertyByQuery($sql);

        if(empty($target_id))
            $target_id = 0;

        //set sintax
        $sql = " SELECT MAX(CAST(value AS SIGNED)) AS value
                 FROM metric
                 WHERE id > $target_id AND namespace = '$namespace'
                 AND device_time >= '$cur_date'";

        //get result query
        $value = Metric::getPropertyByQuery($sql, "value");
        //returns formatted time
        return Dates::formatSecondsToHHMM($value, true);
    }

    /**
     * Continuous Driving Time, get an array of objects with continuous driving time props
     * @param  int $amo_id
     * @param  string $start_date The start date
     * @param  string $end_date   The end date
     * @return array
     */
    public static function getContinuousDrivingTimeForReport($amo_id = 0, $start_date = null, $end_date = null)
    {
        $namespace = Metric::ENGINE_TIME_NAMESPACE;

        //check dates and append time
        if(is_null($start_date))
            $start_date = date(self::CURDATE_FORMAT);

        $start_date .= " ".self::DAY_ZERO_HOUR;

        if(is_null($end_date))
            $end_date = date(self::CURDATE_FORMAT);

        $end_date .= " ".self::DAY_LAST_HOUR;

        $sql = "SELECT id, id AS mid, value, TRUNCATE(value/60, 2) AS cdt_mins, device_time,
                    (DATE_SUB(device_time,INTERVAL TRUNCATE(value, 0) SECOND)) AS start_time,
                    (SELECT id from metric WHERE id > mid AND namespace = '$namespace' LIMIT 0,1) AS next_id,
                    (SELECT value FROM metric WHERE id > mid AND namespace = '$namespace' LIMIT 0,1) AS next_value
                FROM metric
                WHERE amo_id = $amo_id
                    AND namespace = '$namespace'
                    AND device_time BETWEEN '$start_date' AND '$end_date'
                    AND value > 0
                    HAVING cdt_mins > 5
                        AND (next_value = 0 OR next_value IS NULL)";

        $objects = Metric::getByQuery($sql);
        //print_r($sql);exit;

        if(!$objects)
            return [];

        return $objects->count() > 0 ? $objects : [];
    }

    /**
     * Daily Driving Time, the acumulative driven time in a work day
     * @param  int $amo_id
     * @param  string $date The day date, with format: Y-m-d
     * @param  boolean $formatted Outputs result formatted as HH:mm string
     * @return string The formatted output
     */
    public static function getDailyDrivingTime($amo_id = 0, $date = null, $formatted = true)
    {
        $namespace = Metric::ENGINE_TIME_NAMESPACE;
        $a_day     = is_null($date) ? date(self::CURDATE_FORMAT) : $date;

        $curdate_open  = $a_day." ".self::DAY_OPENING_HOUR; //current date start
        $curdate_close = $a_day." ".self::DAY_CLOSING_HOUR; //current date end

        //set sintax
        $sql = " SELECT value
                 FROM metric
                 WHERE namespace = '$namespace'
                 AND device_time BETWEEN '$curdate_open' AND '$curdate_close'";

        //get result query
        $objects = Metric::getByQuery($sql);
        $sum     = self::_getAccumulativeDrivingTimeWithObjects($objects);

        //returns formatted time
        return $formatted ? Dates::formatSecondsToHHMM($sum, true) : $sum;
    }

    /**
     * Daily Rest Time, total rest time in a work day
     * @param  int $amo_id
     * @param  string $date The day date, with format: Y-m-d
     * @param  boolean $formatted Outputs result formatted as HH:mm string
     * @return string The formatted output
     */
    public static function getDailyRestTime($amo_id = 0, $date = null, $formatted = true)
    {
        $namespace = Metric::ENGINE_TIME_NAMESPACE;
        $a_day     = is_null($date) ? date(self::CURDATE_FORMAT) : $date;

        $daily_driving_time = self::getDailyDrivingTime($amo_id, $namespace, $a_day, false);

        //get sum
        $sum = self::DAY_WORKED_TIME - $daily_driving_time;

        if($sum < 0) $sum = 0;

        //returns formatted time
        return $formatted ? Dates::formatSecondsToHHMM($sum, true) : $sum;
    }

    /**
     * Night Driving Time, total night time driven in a day
     * @param  int $amo_id
     * @param  string $date The day date, with format: Y-m-d
     * @param  boolean $formatted Outputs result formatted as HH:mm string
     * @return string The formatted output
     */
    public static function getNightDrivingTime($amo_id = 0, $date = null, $formatted = true)
    {
        $namespace = Metric::ENGINE_TIME_NAMESPACE;
        $a_day     = is_null($date) ? date(self::CURDATE_FORMAT) : $date;

        $time_condition = "BETWEEN '".$a_day." ".self::DAY_ZERO_HOUR."' AND '".$a_day." ".self::DAY_OPENING_HOUR."' OR device_time ";
        $time_condition .= "BETWEEN '".$a_day." ".self::DAY_CLOSING_HOUR."' AND '".$a_day." ".self::DAY_LAST_HOUR."'";

        //set sintax
        $sql = " SELECT value FROM metric
                 WHERE namespace = '$namespace'
                 AND device_time $time_condition";

        //get result query
        $objects = Metric::getByQuery($sql);
        $sum     = self::_getAccumulativeDrivingTimeWithObjects($objects);

        //returns formatted time
        return $formatted ? Dates::formatSecondsToHHMM($sum, true) : $sum;
    }

     /**
     * Harsh Accelerations, get quantity of alarms, alerts and registers for defined levels
     * @param  int $amo_id
     * @param  string $date The day date, with format: Y-m-d
     * @param  unsigned $upper superior range (positive value)
     * @param  unsigned $mid middle range
     * @param  unsigned $lower lower range
     * @param  boolean $is_negative true if range are lower than zero
     * @return list array for values
     */
    private static function getHarshs($amo_id = 0, $date = null, $upper = 0, $middle = 0, $lower = 0, $is_negative = false)
    {
        $namespace = Metric::ACCEL_NAMESPACE;
        $a_day     = is_null($date) ? date(self::CURDATE_FORMAT) : $date;

        $comparator = $is_negative?"<=":">=";
        $lower = $is_negative?$lower*-1:$lower;
        $middle = $is_negative?$middle*-1:$middle;
        $upper = $is_negative?$upper*-1:$upper;

        $time_condition = " AND DATE(device_time) = '$date'";

        $sql_register = "SELECT COUNT(amo_id) as num FROM metric WHERE namespace = '$namespace' AND amo_id = '$amo_id' AND value $comparator $lower";
        $sql_alert    = "SELECT COUNT(amo_id) as num FROM metric WHERE namespace = '$namespace' AND amo_id = '$amo_id' AND value $comparator $middle";
        $sql_alarm    = "SELECT COUNT(amo_id) as num FROM metric WHERE namespace = '$namespace' AND amo_id = '$amo_id' AND value $comparator $upper";

        if(!is_null($date)) {

            $sql_register .= $time_condition;
            $sql_alert    .= $time_condition;
            $sql_alarm    .= $time_condition;
        }

        $registers = Metric::getByQuery($sql_register);
        $alerts    = Metric::getByQuery($sql_alert);
        $alarms    = Metric::getByQuery($sql_alarm);

        $ret = new \stdClass();

        foreach($registers as $register)
            $ret->registers = $register->num;

        foreach($alerts as $alert)
            $ret->alerts = $alert->num;

        foreach($alarms as $alarm)
            $ret->alarms = $alarm->num;

        return $ret;
    }

    /**
     * Harsh Accelerations, get quantity of alarms, alerts and register for positive accelerations
     * @param  int $amo_id
     * @param  string $date The day date, with format: Y-m-d
     * @return list array for values
     */
    public static function getHarshAccelerations($amo_id = 0, $date = null)
    {
        return self::getHarshs($amo_id, $date, 10, 8, 6,false);
    }

     /**
     * Harsh Breakings, get quantity of alarms, alerts and register for negative accelerations
     * @param  int $amo_id
     * @param  string $date The day date, with format: Y-m-d
     * @return list array for values
     */
    public static function getHarshBreakings($amo_id = 0, $date = null)
    {
        return self::getHarshs($amo_id, $date, 14, 12, 10,true);
    }
    /** ------------------------------------------- § -------------------------------------------------- **/

    /**
     * Get the total sum driving time
     * @param  array $objects
     * @return int
     */
    private static function _getAccumulativeDrivingTimeWithObjects($objects = array())
    {
        if(empty($objects))
            return 0;

        $count     = count($objects);
        $last_time = 0;
        $sum       = 0;
        $i         = 0;

        //loop through data
        foreach ($objects as $obj) {

            $time = (int)$obj->value;
            //check last item for non zero
            if(++$i === $count && $time > 0) {
                $sum += $time;
                break;
            }

            //only sum last value before a rest period
            if($time == 0 && $last_time > 0)
                $sum += $last_time;

            $last_time = $time;
        }

        return $sum;
    }

    /**
     * Overspeeding thresholds
     * @TODO:  check correct registers results.
     * @param  int $amo_id The amo ID
     * @param  string $date The day date, with format: Y-m-d
     * @param  unsigned $speed_limit superior range (positive value)
     * @return list array for values
    */
    public static function getOverSpeed($amo_id = 0, $date = null, $speed_limit = 90)
    {
        $namespace = Metric::SPEED_NAMESPACE;
        $a_day     = is_null($date) ? date(self::CURDATE_FORMAT) : $date;

        $value_condition = " AND value >= '".floor($speed_limit*1.05)."' ";
        $time_condition  = " AND DATE(device_time) = '$date'";

        $sql_ids = "SELECT COUNT(*) AS num FROM metric WHERE namespace = 'velocidad'";

        if($speed_limit > 0)
             $sql_ids .= $value_condition;

        if($date != null)
            $sql_ids .= $time_condition;

        $total = Metric::getByQuery($sql_ids);

        $max_ids = $total[0]->num;
        $pages   = floor($max_ids/12);

        $ret = new \stdClass();
        $ret->registers = $ret->alerts = $ret->alarms = 0;
        $ret->last_value = 0;

        // find registers
        for($i = 0; $i <= $pages; $i++){

            $sql_ids = "SELECT * FROM
                        (SELECT value, CAST(value AS UNSIGNED) AS real_value,
                        namespace, device_time FROM metric) AS fix_metrics
                        WHERE namespace = 'velocidad'";

            if($speed_limit > 0)
                $sql_ids .= " AND real_value >= '".floor($speed_limit*1.05)."' ";

            if($date != null)
                $sql_ids .= $time_condition;

            $sql_ids .= " limit ".($i*12).", 12";

            $metrics = Metric::getByQuery($sql_ids);

            if(!isset($metrics[0]))
                continue;

            foreach($metrics as $metric) {

                if($metric->real_value >= $speed_limit*1.2) { // find alarms
                    $ret->alarms++;
                }
                else if($metric->real_value >= $speed_limit*1.1) { // find alerts
                    $ret->alerts++;
                }
                else if($metric->real_value >= $speed_limit*1.05)
                    $ret->registers++;

                $ret->last_value = $metric->real_value;
            }
        }

        return $ret;
    }
}
