<?php

use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\StringLength;
//other imports
use CrazyCake\Models\Base;
use CrazyCake\Helpers\GPS;

class Amo extends Base
{
    /* properties */

    /**
     * @var string
     */
    public $name;

    /**
     * value inlusion in AMO_TYPES
     * @var string
     */
    public $type;

    /**
     * timestamp
     * @var string
     */
    public $created_at;

    /* inclusion vars */

    static $AMO_TYPES = ['truck', 'machine', 'car'];

    /* custom props */

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    /**
     * Initializer
     */
    public function initialize()
    {
        //Skips fields/columns on both INSERT/UPDATE operations
        $this->skipAttributes(['created_at']);

        //model relations
        $this->hasMany("id", "Metric", "amo_id");
    }

    /**
     * After Fetch Event
     */
    public function afterFetch()
    {
        //set amo last location
        $this->_setLastLocation();
    }

    /**
     * Validation
     */
    public function validation()
    {
        //type
        $this->validate( new InclusionIn([
            "field"   => "type",
            "domain"  => self::$AMO_TYPES,
            "message" => "invalid data type, possible values: ".implode(", ", self::$AMO_TYPES)
        ]));

        //name length
        $this->validate(new StringLength([
            'field' => 'name',
            'max'   => 90,
            'min'   => 3,
            'messageMaximum' => 'Name length too long',
            'messageMinimum' => 'Name length too short'
        ]));

        //check validations
        if ($this->validationHasFailed() == true)
            return false;
    }

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * Late static binding
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string The current class name
     */
    public static function who()
    {
        return __CLASS__;
    }

    /**
     * Get amos with conditions & limit results
     * @access public
     * @static
     * @param string $conditions
     * @param array $limit
     * @return array
     */
    public static function getByConditions($conditions = array(), $limit = array())
    {
        if(!empty($conditions))
            $conditions = "WHERE ".$conditions;

        return self::getByQuery(
            "SELECT *
             FROM amo
             $conditions
             LIMIT ".(int)$limit['offset'].", ".(int)$limit['number']
        );
    }

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * Set last amo location by searching coordinates metrics in database
     * @param  boolean $use_coordinate_namespace Search by a full coordinate measure,
     * instead of separated latitude-longitude unit style.
     */
    private function _setLastLocation()
    {
        //get last coordinate
        $result = Metric::find([
            "conditions" => "amo_id = '".$this->id."' AND namespace = '".Metric::COORDINATE_NAMESPACE."'",
            "columns"    => "value",
            "order"      => "created_at DESC",
            "limit"      => 1
        ]);
        //parse coordinate
        $coord = $result->getFirst();
        $coord = empty($coord->value) ? [null, null] : explode(",", $coord->value);
        //set values (data is already formated)
        list($this->latitude, $this->longitude) = $coord;
        return;
    }
}
