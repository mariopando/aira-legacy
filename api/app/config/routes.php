<?php
/**
 * Phalcon App Routes files
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

return function($application) {
	//welcome message
	$application->get('/',[new WsCoreController(), "welcome"]);

	//upload text file
	$application->post('/upload/file',[new WsUploadController(), "uploadFile"]);

	//save a batch of metric
	$application->post('/metrics/save_batch',[new WsMetricController(), "newBatch"]);

	//get data from metrics
	$application->get('/metrics/get',[new WsMetricController(), "getMetrics"]);

	//get metrics namespaces
	$application->get('/metrics/namespaces',[new WsMetricController(), "getNamespaces"]);

	//dump metrics
	$application->get('/metrics/dump',[new WsMetricController(), "dumpMetrics"]);

	//save an AMO
	$application->post('/amo/new',[new WsAmoController(), "newAmo"]);

	//get AMOS near me
	$application->get('/amo/get',[new WsAmoController(), "getAmo"]);

	//not found handler
	$application->notFound( function() use (&$application) {
		//$application->response->setStatusCode(404, "Not Found")->sendHeaders();
		$service = new WsCoreController();
		$service->serviceNotFound();
	});
};
