<?php
/**
 * CLI Main Task: main tasks for CLI commands
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//Jp Graph
use JpGraph\JpGraph;
//CrazyCake
use CrazyCake\Core\TaskCore;

class ReportTask extends TaskCore
{
    /* consts */
    const REPORTS_PATH = MODULE_PATH."outputs/";
    const DATE_FORMAT  = 'Y-m-d';

    /**
     * Main Action
     */
    public function mainAction()
    {
        $this->_colorize("Please run 'php cli/cli.php main' for usage commands", "WARNING");
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Report, generates a metric report
     * @param string [amo_id] The Amo ID
     * @param string [start_date] The starting date, format set in const var.
     * @return mixed
     */
    public function buildReport($params = array())
    {
        list($amo, $start_datetime, $days_forward) = $this->_setupReport($params);

        //set data for queries
        $data = [
            'amo_id' => $amo->id
        ];
        //vars
        $max_speed        = 90;
        $data_harsh_accel = [];
        $data_harsh_break = [];

        //++ Outputs ----------------------------------------------------

        //loop through days
        for ($i = 0; $i < $days_forward; $i++) {

            $this->_colorize("\nDay: ".$start_datetime->format('D d-M-Y'), "NOTE");

            //set date for params
            $start_date         = $start_datetime->format(self::DATE_FORMAT);
            $data['date']       = $start_date;
            $data['fixed_date'] = TRUE;


            //set metric
            $data['namespace'] = Metric::SPEED_NAMESPACE;
            $metric = Metric::getMetricsByData($data);

            //output count
            $this->_colorize("var ".$data['namespace'].", count: ".count($metric));

            if(count($metric) > 0) {
                $this->_makePlot($metric, "plot_".$data['namespace']."_day_".$start_datetime->format('d-m-Y'),120);
            }

            //set metric
            $data['namespace'] = Metric::RPM_NAMESPACE;
            $metric            = Metric::getMetricsByData($data);

            //output count
            $this->_colorize("var ".$data['namespace'].", count: ".count($metric));

            if(count($metric)>0){
                $this->_makePlot($metric, "plot_".$data['namespace']."_day_".$start_datetime->format('d-m-Y'),3200);
            }

            //get cdt, breakings for report
            $cdts = Kpi::getContinuousDrivingTimeForReport($amo->id, $start_date, $start_date);

            //output count
            $this->_colorize("var cdts, count: ".count($cdts));

            //loop objects
            foreach ($cdts as $kpi) {
                $this->_colorize("\tid: $kpi->id, cdt_mins: $kpi->cdt_mins, start_time: $kpi->start_time, device_time: $kpi->device_time");
            }

            //get harsh breaking/acceleration alerts/alarms/registers
            $acceleration = Kpi::getHarshAccelerations($amo->id,$start_date);
            $breaking     = Kpi::getHarshBreakings($amo->id,$start_date);

            $overspeed = Kpi::getOverSpeed($amo->id, $start_date, $max_speed);

            $this->_colorize("var overspeed: (max speed:$max_speed)");
            $this->_colorize("\tRegister: $overspeed->registers, Alerts: $overspeed->alerts, Alarms: $overspeed->alarms");

            $data_harsh_accel[$start_date] = $acceleration;
            $data_harsh_break[$start_date] = $breaking;

            $this->_colorize("var accelerations: ");
            $this->_colorize("\tRegister: $acceleration->registers, Alerts: $acceleration->alerts, Alarms: $acceleration->alarms");

            $this->_colorize("var breakings: ");
            $this->_colorize("\tRegister: $breaking->registers, Alerts: $breaking->alerts, Alarms: $breaking->alarms");

            //sum 1 day to datetime
            $start_datetime->modify('+1 day');
        }

        //plot harsh alerts
        $this->_makeHarshPlot($data_harsh_accel, "Harsh Acceleration", "plot_harsh_accel_week");
        $this->_makeHarshPlot($data_harsh_break, "Harsh Breaking", "plot_harsh_break_week");
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Renders a plot
     * @link http://jpgraph.net/download/manuals/chunkhtml/ch04s02.html [Examples & doc]
     * @param  array $objects The given objects
     * @param  string $filename The output filename
     * @return void
     */
    private function _makePlot($objects, $filename, $upper_limit = null)
    {
        $xdata = array();
        $ydata = array();

        $y_type = $objects[0]['namespace'];
        $amo_id = $objects[0]['amo_id'];
        $y_date = date("Y-m-d",strtotime($objects[0]['device_time']));

        foreach ($objects as $obj) {
            array_push($xdata, strtotime($obj['device_time']));
            array_push($ydata, $obj['value']);
        }

        /*if($y_type == Metric::SPEED_NAMESPACE){

        }*/

        // Create the linear plot
        $lineplot = new LinePlot($ydata,$xdata);
        $lineplot->SetColor('navy');
        $lineplot->SetLegend($y_type);
        // Add the plot to the graph


        $graph = new Graph(1136, 640);
        $graph->SetMargin(40,40,30,130);
        $graph->SetScale('datlin');
        $graph->title->Set($y_type."@".$y_date);
        $graph->xaxis->SetLabelAngle(90);

        $graph->Add($lineplot);

        if($y_type == Metric::SPEED_NAMESPACE) {

            $ylimit = array();

            foreach($xdata as $i)
                $ylimit[] = 90;

            $ha = Kpi::getHarshAccelerations($amo_id,$y_date);

            $lineplot_limit = new LinePlot($ylimit,$xdata);
            $lineplot_limit->SetLegend("MAX_LIMIT");
            $lineplot_limit->SetColor('red');

            $txt = [
                [$ha->registers,"green"],
                [$ha->alerts,"red"],
                [$ha->alarms,"blue"]
            ];

            $txtbox = array();

            for($i = 0; $i < count($txt); ++$i ) {

                $txtbox[$i] = new Text($txt[$i][0]);
                $txtbox[$i]->SetPos(0.1,0.1*($i+1),0.9,'right');
                $txtbox[$i]->SetParagraphAlign('right');
                $txtbox[$i]->SetColor($txt[$i][1]);
                $txtbox[$i]->SetBox();
            }
            //TODO: draw legend
            //$graph->Add($txtbox);

            $graph->Add($lineplot_limit);
        }

        if($upper_limit != null){
            $graph->yaxis->scale->SetAutoMax($upper_limit);
            $graph->yaxis->scale->SetAutoMin(0);
        }

        $graph->legend->SetShadow('gray@0.4',5);
        $graph->legend->SetPos(0.1,0.1,'left','top');

        //render the graph
        $graph->img->SetImgFormat('jpeg');
        $graph->Stroke(_IMG_HANDLER); //jpGraph costant
        //save to disk
        $graph->img->Stream(self::REPORTS_PATH.$filename.".jpg");
    }

    /**
     * Create bar accumulative graph
     * @param  array $objects The given objects
     * @param  string $filename The output filename
     */
    private function _makeHarshPlot($objects, $title, $filename)
    {
        $xdata = $y1data = $y2data = $y3data = array();

        foreach ($objects as $day => $obj) {

            array_push($y1data,$obj->registers);
            array_push($y2data,$obj->alerts);
            array_push($y3data,$obj->alarms);

            array_push($xdata, $day);
        }

        $graph = new Graph(1136, 640);
        $graph->SetScale('textint');
        $graph->title->Set($title);
        $graph->SetMargin(40,30,20,40);
        $graph->xaxis->SetTickLabels($xdata);

        // Create two bar plots
        $b1plot = new BarPlot($y1data);
        $b1plot->SetFillColor('green');
        $b2plot = new BarPlot($y2data);
        $b2plot->SetFillColor('yellow');
        $b3plot = new BarPlot($y3data);
        $b3plot->SetFillColor('red');

        $gbplot = new GroupBarPlot(array($b1plot,$b2plot,$b3plot));
        // Add the accumulated plot to the graph
        $graph->Add($gbplot);

        //render the graph
        $graph->img->SetImgFormat('jpeg');
        $graph->Stroke(_IMG_HANDLER); //jpGraph costant
        //save to disk
        $graph->img->Stream(self::REPORTS_PATH.$filename.".jpg");
    }

    /**
     * Handles report input parameters
     * @param  array  $params The input array
     * @return array
     */
    private function _setupReport($params = array())
    {
        if(!isset($params[0]))
            $this->_colorize("amo_id is required", "ERROR", true);

        if(!isset($params[1]) || strpos($params[1], "-") === false)
            $this->_colorize("start_date is required, format: ".self::DATE_FORMAT, "ERROR", true);

        //check if amo_id exists
        $amo = Amo::getById($params[0]);
        if(!$amo)
            $this->_colorize("given amo_id don't exists...", "WARNING");

        //creates folder if not exists
        if(!is_dir(self::REPORTS_PATH))
            mkdir(self::REPORTS_PATH, 0775);

        //loads jpGraph modules
        JpGraph::load();
        JpGraph::module('line');
        JpGraph::module('bar');
        JpGraph::module('date'); // for DateScale

        //set common vars
        $start_datetime = DateTime::createFromFormat(self::DATE_FORMAT, $params[1]);
        $days_forward   = isset($params[2]) ? (int)$params[2] : 1;

        return array($amo, $start_datetime, $days_forward);
    }
}
