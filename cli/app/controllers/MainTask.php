<?php
/**
 * CLI Main Task: main tasks for CLI commands
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake
use CrazyCake\Core\TaskCore;

class MainTask extends TaskCore
{
    /**
     * Main Action Executer
     */
    public function mainAction()
    {
        parent::mainAction();

        $this->_colorize("report [amo_id] [start_date] [days_forward] -> Calculates metric reports", "WARNING");
    }

    /**
     * Report, generates a metric report
     * @param string [amo_id] The Amo ID
     * @param string [start_date] The starting date
     * @return mixed
     */
    public function reportAction($params = array())
    {
        (new ReportTask())->buildReport($params);
    }
}
